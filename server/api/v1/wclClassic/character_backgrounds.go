package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
    "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
    "github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
    wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
    "github.com/flipped-aurora/gin-vue-admin/server/model/common/response"
    "github.com/flipped-aurora/gin-vue-admin/server/service"
    "github.com/gin-gonic/gin"
    "go.uber.org/zap"
)

type CharacterBackgroundsApi struct {
}

var characterBackgroundsService = service.ServiceGroupApp.WclClassicServiceGroup.CharacterBackgroundsService


// CreateCharacterBackgrounds 创建CharacterBackgrounds
// @Tags CharacterBackgrounds
// @Summary 创建CharacterBackgrounds
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.CharacterBackgrounds true "创建CharacterBackgrounds"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /characterBackgrounds/createCharacterBackgrounds [post]
func (characterBackgroundsApi *CharacterBackgroundsApi) CreateCharacterBackgrounds(c *gin.Context) {
	var characterBackgrounds wclClassic.CharacterBackgrounds
	err := c.ShouldBindJSON(&characterBackgrounds)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := characterBackgroundsService.CreateCharacterBackgrounds(&characterBackgrounds); err != nil {
        global.GVA_LOG.Error("创建失败!", zap.Error(err))
		response.FailWithMessage("创建失败", c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// DeleteCharacterBackgrounds 删除CharacterBackgrounds
// @Tags CharacterBackgrounds
// @Summary 删除CharacterBackgrounds
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.CharacterBackgrounds true "删除CharacterBackgrounds"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /characterBackgrounds/deleteCharacterBackgrounds [delete]
func (characterBackgroundsApi *CharacterBackgroundsApi) DeleteCharacterBackgrounds(c *gin.Context) {
	var characterBackgrounds wclClassic.CharacterBackgrounds
	err := c.ShouldBindJSON(&characterBackgrounds)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := characterBackgroundsService.DeleteCharacterBackgrounds(characterBackgrounds); err != nil {
        global.GVA_LOG.Error("删除失败!", zap.Error(err))
		response.FailWithMessage("删除失败", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// DeleteCharacterBackgroundsByIds 批量删除CharacterBackgrounds
// @Tags CharacterBackgrounds
// @Summary 批量删除CharacterBackgrounds
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除CharacterBackgrounds"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"批量删除成功"}"
// @Router /characterBackgrounds/deleteCharacterBackgroundsByIds [delete]
func (characterBackgroundsApi *CharacterBackgroundsApi) DeleteCharacterBackgroundsByIds(c *gin.Context) {
	var IDS request.IdsReq
    err := c.ShouldBindJSON(&IDS)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := characterBackgroundsService.DeleteCharacterBackgroundsByIds(IDS); err != nil {
        global.GVA_LOG.Error("批量删除失败!", zap.Error(err))
		response.FailWithMessage("批量删除失败", c)
	} else {
		response.OkWithMessage("批量删除成功", c)
	}
}

// UpdateCharacterBackgrounds 更新CharacterBackgrounds
// @Tags CharacterBackgrounds
// @Summary 更新CharacterBackgrounds
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.CharacterBackgrounds true "更新CharacterBackgrounds"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /characterBackgrounds/updateCharacterBackgrounds [put]
func (characterBackgroundsApi *CharacterBackgroundsApi) UpdateCharacterBackgrounds(c *gin.Context) {
	var characterBackgrounds wclClassic.CharacterBackgrounds
	err := c.ShouldBindJSON(&characterBackgrounds)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := characterBackgroundsService.UpdateCharacterBackgrounds(characterBackgrounds); err != nil {
        global.GVA_LOG.Error("更新失败!", zap.Error(err))
		response.FailWithMessage("更新失败", c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// FindCharacterBackgrounds 用id查询CharacterBackgrounds
// @Tags CharacterBackgrounds
// @Summary 用id查询CharacterBackgrounds
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query wclClassic.CharacterBackgrounds true "用id查询CharacterBackgrounds"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /characterBackgrounds/findCharacterBackgrounds [get]
func (characterBackgroundsApi *CharacterBackgroundsApi) FindCharacterBackgrounds(c *gin.Context) {
	var characterBackgrounds wclClassic.CharacterBackgrounds
	err := c.ShouldBindQuery(&characterBackgrounds)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if recharacterBackgrounds, err := characterBackgroundsService.GetCharacterBackgrounds(characterBackgrounds.ID); err != nil {
        global.GVA_LOG.Error("查询失败!", zap.Error(err))
		response.FailWithMessage("查询失败", c)
	} else {
		response.OkWithData(gin.H{"recharacterBackgrounds": recharacterBackgrounds}, c)
	}
}

// GetCharacterBackgroundsList 分页获取CharacterBackgrounds列表
// @Tags CharacterBackgrounds
// @Summary 分页获取CharacterBackgrounds列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query wclClassicReq.CharacterBackgroundsSearch true "分页获取CharacterBackgrounds列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /characterBackgrounds/getCharacterBackgroundsList [get]
func (characterBackgroundsApi *CharacterBackgroundsApi) GetCharacterBackgroundsList(c *gin.Context) {
	var pageInfo wclClassicReq.CharacterBackgroundsSearch
	err := c.ShouldBindQuery(&pageInfo)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if list, total, err := characterBackgroundsService.GetCharacterBackgroundsInfoList(pageInfo); err != nil {
	    global.GVA_LOG.Error("获取失败!", zap.Error(err))
        response.FailWithMessage("获取失败", c)
    } else {
        response.OkWithDetailed(response.PageResult{
            List:     list,
            Total:    total,
            Page:     pageInfo.Page,
            PageSize: pageInfo.PageSize,
        }, "获取成功", c)
    }
}
