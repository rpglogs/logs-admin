package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/response"
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
	"github.com/flipped-aurora/gin-vue-admin/server/service"
	"github.com/flipped-aurora/gin-vue-admin/server/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

type LotteryActivityApi struct {
}

var lotteryActivityService = service.ServiceGroupApp.WclClassicServiceGroup.LotteryActivityService

// CreateLotteryActivity 创建LotteryActivity
// @Tags LotteryActivity
// @Summary 创建LotteryActivity
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.LotteryActivity true "创建LotteryActivity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /lotteryActivity/createLotteryActivity [post]
func (lotteryActivityApi *LotteryActivityApi) CreateLotteryActivity(c *gin.Context) {
	var lotteryActivity wclClassic.LotteryActivity
	err := c.ShouldBindJSON(&lotteryActivity)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	verify := utils.Rules{
		"Name":         {utils.NotEmpty()},
		"ActivityType": {utils.NotEmpty()},
		"VendorId":     {utils.NotEmpty()},
		"Period":       {utils.NotEmpty()},
		"StartAt":      {utils.NotEmpty()},
		"EndAt":        {utils.NotEmpty()},
		"PrizeId":      {utils.NotEmpty()},
		"PrizeNum":     {utils.NotEmpty()},
	}
	if err := utils.Verify(lotteryActivity, verify); err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := lotteryActivityService.CreateLotteryActivity(&lotteryActivity); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Error(err))
		response.FailWithMessage("创建失败", c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// DeleteLotteryActivity 删除LotteryActivity
// @Tags LotteryActivity
// @Summary 删除LotteryActivity
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.LotteryActivity true "删除LotteryActivity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /lotteryActivity/deleteLotteryActivity [delete]
func (lotteryActivityApi *LotteryActivityApi) DeleteLotteryActivity(c *gin.Context) {
	var lotteryActivity wclClassic.LotteryActivity
	err := c.ShouldBindJSON(&lotteryActivity)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := lotteryActivityService.DeleteLotteryActivity(lotteryActivity); err != nil {
		global.GVA_LOG.Error("删除失败!", zap.Error(err))
		response.FailWithMessage("删除失败", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// DeleteLotteryActivityByIds 批量删除LotteryActivity
// @Tags LotteryActivity
// @Summary 批量删除LotteryActivity
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除LotteryActivity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"批量删除成功"}"
// @Router /lotteryActivity/deleteLotteryActivityByIds [delete]
func (lotteryActivityApi *LotteryActivityApi) DeleteLotteryActivityByIds(c *gin.Context) {
	var IDS request.IdsReq
	err := c.ShouldBindJSON(&IDS)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := lotteryActivityService.DeleteLotteryActivityByIds(IDS); err != nil {
		global.GVA_LOG.Error("批量删除失败!", zap.Error(err))
		response.FailWithMessage("批量删除失败", c)
	} else {
		response.OkWithMessage("批量删除成功", c)
	}
}

// UpdateLotteryActivity 更新LotteryActivity
// @Tags LotteryActivity
// @Summary 更新LotteryActivity
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.LotteryActivity true "更新LotteryActivity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /lotteryActivity/updateLotteryActivity [put]
func (lotteryActivityApi *LotteryActivityApi) UpdateLotteryActivity(c *gin.Context) {
	var lotteryActivity wclClassic.LotteryActivity
	err := c.ShouldBindJSON(&lotteryActivity)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	verify := utils.Rules{
		"Name":         {utils.NotEmpty()},
		"ActivityType": {utils.NotEmpty()},
		"VendorId":     {utils.NotEmpty()},
		"Period":       {utils.NotEmpty()},
		"StartAt":      {utils.NotEmpty()},
		"EndAt":        {utils.NotEmpty()},
	}
	if err := utils.Verify(lotteryActivity, verify); err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := lotteryActivityService.UpdateLotteryActivity(lotteryActivity); err != nil {
		global.GVA_LOG.Error("更新失败!", zap.Error(err))
		response.FailWithMessage("更新失败", c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// FindLotteryActivity 用id查询LotteryActivity
// @Tags LotteryActivity
// @Summary 用id查询LotteryActivity
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query wclClassic.LotteryActivity true "用id查询LotteryActivity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /lotteryActivity/findLotteryActivity [get]
func (lotteryActivityApi *LotteryActivityApi) FindLotteryActivity(c *gin.Context) {
	var lotteryActivity wclClassic.LotteryActivity
	err := c.ShouldBindQuery(&lotteryActivity)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if relotteryActivity, err := lotteryActivityService.GetLotteryActivity(lotteryActivity.ID); err != nil {
		global.GVA_LOG.Error("查询失败!", zap.Error(err))
		response.FailWithMessage("查询失败", c)
	} else {
		response.OkWithData(gin.H{"relotteryActivity": relotteryActivity}, c)
	}
}

// GetLotteryActivityList 分页获取LotteryActivity列表
// @Tags LotteryActivity
// @Summary 分页获取LotteryActivity列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query wclClassicReq.LotteryActivitySearch true "分页获取LotteryActivity列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /lotteryActivity/getLotteryActivityList [get]
func (lotteryActivityApi *LotteryActivityApi) GetLotteryActivityList(c *gin.Context) {
	var pageInfo wclClassicReq.LotteryActivitySearch
	err := c.ShouldBindQuery(&pageInfo)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if list, total, err := lotteryActivityService.GetLotteryActivityInfoList(pageInfo); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Error(err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}
