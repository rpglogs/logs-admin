package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/response"
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
	"github.com/flipped-aurora/gin-vue-admin/server/service"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

type CdkeyApi struct {
}

var cdkeyService = service.ServiceGroupApp.WclClassicServiceGroup.CdkeyService

// CreateCdkey 创建Cdkey
// @Tags Cdkey
// @Summary 创建Cdkey
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.Cdkey true "创建Cdkey"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cdkey/createCdkey [post]
func (cdkeyApi *CdkeyApi) CreateCdkey(c *gin.Context) {
	var cdkey wclClassic.Cdkey
	err := c.ShouldBindJSON(&cdkey)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	game := c.GetHeader("X-game")
	if err := cdkeyService.CreateCdkey(&cdkey, game); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Error(err))
		response.FailWithMessage("创建失败", c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

func (cdkeyApi *CdkeyApi) BatchCreateCdkeys(c *gin.Context) {
	var request wclClassicReq.BatchCreateCdkeys
	err := c.ShouldBindJSON(&request)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	game := c.GetHeader("X-game")
	if list, err := cdkeyService.BatchCreateCdkeys(&request, game); err != nil {
		global.GVA_LOG.Error("批量创建失败!", zap.Error(err))
		response.FailWithMessage("批量创建失败", c)
	} else {
		response.OkWithData(list, c)
	}
}

// DeleteCdkey 删除Cdkey
// @Tags Cdkey
// @Summary 删除Cdkey
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.Cdkey true "删除Cdkey"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /cdkey/deleteCdkey [delete]
func (cdkeyApi *CdkeyApi) DeleteCdkey(c *gin.Context) {
	var cdkey wclClassic.Cdkey
	err := c.ShouldBindJSON(&cdkey)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	game := c.GetHeader("X-game")
	if err := cdkeyService.DeleteCdkey(cdkey, game); err != nil {
		global.GVA_LOG.Error("删除失败!", zap.Error(err))
		response.FailWithMessage("删除失败", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// DeleteCdkeyByIds 批量删除Cdkey
// @Tags Cdkey
// @Summary 批量删除Cdkey
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除Cdkey"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"批量删除成功"}"
// @Router /cdkey/deleteCdkeyByIds [delete]
func (cdkeyApi *CdkeyApi) DeleteCdkeyByIds(c *gin.Context) {
	var IDS request.IdsReq
	err := c.ShouldBindJSON(&IDS)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	game := c.GetHeader("X-game")
	if err := cdkeyService.DeleteCdkeyByIds(IDS, game); err != nil {
		global.GVA_LOG.Error("批量删除失败!", zap.Error(err))
		response.FailWithMessage("批量删除失败", c)
	} else {
		response.OkWithMessage("批量删除成功", c)
	}
}

// UpdateCdkey 更新Cdkey
// @Tags Cdkey
// @Summary 更新Cdkey
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.Cdkey true "更新Cdkey"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /cdkey/updateCdkey [put]
func (cdkeyApi *CdkeyApi) UpdateCdkey(c *gin.Context) {
	var cdkey wclClassic.Cdkey
	err := c.ShouldBindJSON(&cdkey)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	game := c.GetHeader("X-game")
	if err := cdkeyService.UpdateCdkey(cdkey, game); err != nil {
		global.GVA_LOG.Error("更新失败!", zap.Error(err))
		response.FailWithMessage("更新失败", c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// FindCdkey 用id查询Cdkey
// @Tags Cdkey
// @Summary 用id查询Cdkey
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query wclClassic.Cdkey true "用id查询Cdkey"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /cdkey/findCdkey [get]
func (cdkeyApi *CdkeyApi) FindCdkey(c *gin.Context) {
	var cdkey wclClassic.Cdkey
	err := c.ShouldBindQuery(&cdkey)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	game := c.GetHeader("X-game")
	if recdkey, err := cdkeyService.GetCdkey(cdkey.ID, game); err != nil {
		global.GVA_LOG.Error("查询失败!", zap.Error(err))
		response.FailWithMessage("查询失败", c)
	} else {
		response.OkWithData(gin.H{"recdkey": recdkey}, c)
	}
}

// GetCdkeyList 分页获取Cdkey列表
// @Tags Cdkey
// @Summary 分页获取Cdkey列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query wclClassicReq.CdkeySearch true "分页获取Cdkey列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cdkey/getCdkeyList [get]
func (cdkeyApi *CdkeyApi) GetCdkeyList(c *gin.Context) {
	var pageInfo wclClassicReq.CdkeySearch
	err := c.ShouldBindQuery(&pageInfo)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	game := c.GetHeader("X-game")
	if list, total, err := cdkeyService.GetCdkeyInfoList(pageInfo, game); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Error(err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}
