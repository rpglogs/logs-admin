package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
    "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
    "github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
    wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
    "github.com/flipped-aurora/gin-vue-admin/server/model/common/response"
    "github.com/flipped-aurora/gin-vue-admin/server/service"
    "github.com/gin-gonic/gin"
    "go.uber.org/zap"
)

type LotteryActivityWinRecordApi struct {
}

var lotteryActivityWinRecordService = service.ServiceGroupApp.WclClassicServiceGroup.LotteryActivityWinRecordService


// CreateLotteryActivityWinRecord 创建LotteryActivityWinRecord
// @Tags LotteryActivityWinRecord
// @Summary 创建LotteryActivityWinRecord
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.LotteryActivityWinRecord true "创建LotteryActivityWinRecord"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /lotteryActivityWinRecord/createLotteryActivityWinRecord [post]
func (lotteryActivityWinRecordApi *LotteryActivityWinRecordApi) CreateLotteryActivityWinRecord(c *gin.Context) {
	var lotteryActivityWinRecord wclClassic.LotteryActivityWinRecord
	err := c.ShouldBindJSON(&lotteryActivityWinRecord)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := lotteryActivityWinRecordService.CreateLotteryActivityWinRecord(&lotteryActivityWinRecord); err != nil {
        global.GVA_LOG.Error("创建失败!", zap.Error(err))
		response.FailWithMessage("创建失败", c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// DeleteLotteryActivityWinRecord 删除LotteryActivityWinRecord
// @Tags LotteryActivityWinRecord
// @Summary 删除LotteryActivityWinRecord
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.LotteryActivityWinRecord true "删除LotteryActivityWinRecord"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /lotteryActivityWinRecord/deleteLotteryActivityWinRecord [delete]
func (lotteryActivityWinRecordApi *LotteryActivityWinRecordApi) DeleteLotteryActivityWinRecord(c *gin.Context) {
	var lotteryActivityWinRecord wclClassic.LotteryActivityWinRecord
	err := c.ShouldBindJSON(&lotteryActivityWinRecord)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := lotteryActivityWinRecordService.DeleteLotteryActivityWinRecord(lotteryActivityWinRecord); err != nil {
        global.GVA_LOG.Error("删除失败!", zap.Error(err))
		response.FailWithMessage("删除失败", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// DeleteLotteryActivityWinRecordByIds 批量删除LotteryActivityWinRecord
// @Tags LotteryActivityWinRecord
// @Summary 批量删除LotteryActivityWinRecord
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除LotteryActivityWinRecord"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"批量删除成功"}"
// @Router /lotteryActivityWinRecord/deleteLotteryActivityWinRecordByIds [delete]
func (lotteryActivityWinRecordApi *LotteryActivityWinRecordApi) DeleteLotteryActivityWinRecordByIds(c *gin.Context) {
	var IDS request.IdsReq
    err := c.ShouldBindJSON(&IDS)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := lotteryActivityWinRecordService.DeleteLotteryActivityWinRecordByIds(IDS); err != nil {
        global.GVA_LOG.Error("批量删除失败!", zap.Error(err))
		response.FailWithMessage("批量删除失败", c)
	} else {
		response.OkWithMessage("批量删除成功", c)
	}
}

// UpdateLotteryActivityWinRecord 更新LotteryActivityWinRecord
// @Tags LotteryActivityWinRecord
// @Summary 更新LotteryActivityWinRecord
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.LotteryActivityWinRecord true "更新LotteryActivityWinRecord"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /lotteryActivityWinRecord/updateLotteryActivityWinRecord [put]
func (lotteryActivityWinRecordApi *LotteryActivityWinRecordApi) UpdateLotteryActivityWinRecord(c *gin.Context) {
	var lotteryActivityWinRecord wclClassic.LotteryActivityWinRecord
	err := c.ShouldBindJSON(&lotteryActivityWinRecord)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := lotteryActivityWinRecordService.UpdateLotteryActivityWinRecord(lotteryActivityWinRecord); err != nil {
        global.GVA_LOG.Error("更新失败!", zap.Error(err))
		response.FailWithMessage("更新失败", c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// FindLotteryActivityWinRecord 用id查询LotteryActivityWinRecord
// @Tags LotteryActivityWinRecord
// @Summary 用id查询LotteryActivityWinRecord
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query wclClassic.LotteryActivityWinRecord true "用id查询LotteryActivityWinRecord"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /lotteryActivityWinRecord/findLotteryActivityWinRecord [get]
func (lotteryActivityWinRecordApi *LotteryActivityWinRecordApi) FindLotteryActivityWinRecord(c *gin.Context) {
	var lotteryActivityWinRecord wclClassic.LotteryActivityWinRecord
	err := c.ShouldBindQuery(&lotteryActivityWinRecord)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if relotteryActivityWinRecord, err := lotteryActivityWinRecordService.GetLotteryActivityWinRecord(lotteryActivityWinRecord.ID); err != nil {
        global.GVA_LOG.Error("查询失败!", zap.Error(err))
		response.FailWithMessage("查询失败", c)
	} else {
		response.OkWithData(gin.H{"relotteryActivityWinRecord": relotteryActivityWinRecord}, c)
	}
}

// GetLotteryActivityWinRecordList 分页获取LotteryActivityWinRecord列表
// @Tags LotteryActivityWinRecord
// @Summary 分页获取LotteryActivityWinRecord列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query wclClassicReq.LotteryActivityWinRecordSearch true "分页获取LotteryActivityWinRecord列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /lotteryActivityWinRecord/getLotteryActivityWinRecordList [get]
func (lotteryActivityWinRecordApi *LotteryActivityWinRecordApi) GetLotteryActivityWinRecordList(c *gin.Context) {
	var pageInfo wclClassicReq.LotteryActivityWinRecordSearch
	err := c.ShouldBindQuery(&pageInfo)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if list, total, err := lotteryActivityWinRecordService.GetLotteryActivityWinRecordInfoList(pageInfo); err != nil {
	    global.GVA_LOG.Error("获取失败!", zap.Error(err))
        response.FailWithMessage("获取失败", c)
    } else {
        response.OkWithDetailed(response.PageResult{
            List:     list,
            Total:    total,
            Page:     pageInfo.Page,
            PageSize: pageInfo.PageSize,
        }, "获取成功", c)
    }
}
