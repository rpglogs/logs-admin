package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
    "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
    "github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
    wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
    "github.com/flipped-aurora/gin-vue-admin/server/model/common/response"
    "github.com/flipped-aurora/gin-vue-admin/server/service"
    "github.com/gin-gonic/gin"
    "go.uber.org/zap"
    "github.com/flipped-aurora/gin-vue-admin/server/utils"
)

type LotteryPrizeVendorApi struct {
}

var lotteryPrizeVendorService = service.ServiceGroupApp.WclClassicServiceGroup.LotteryPrizeVendorService


// CreateLotteryPrizeVendor 创建LotteryPrizeVendor
// @Tags LotteryPrizeVendor
// @Summary 创建LotteryPrizeVendor
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.LotteryPrizeVendor true "创建LotteryPrizeVendor"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /lotteryPrizeVendor/createLotteryPrizeVendor [post]
func (lotteryPrizeVendorApi *LotteryPrizeVendorApi) CreateLotteryPrizeVendor(c *gin.Context) {
	var lotteryPrizeVendor wclClassic.LotteryPrizeVendor
	err := c.ShouldBindJSON(&lotteryPrizeVendor)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
    verify := utils.Rules{
        "Name":{utils.NotEmpty()},
    }
	if err := utils.Verify(lotteryPrizeVendor, verify); err != nil {
    		response.FailWithMessage(err.Error(), c)
    		return
    	}
	if err := lotteryPrizeVendorService.CreateLotteryPrizeVendor(&lotteryPrizeVendor); err != nil {
        global.GVA_LOG.Error("创建失败!", zap.Error(err))
		response.FailWithMessage("创建失败", c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// DeleteLotteryPrizeVendor 删除LotteryPrizeVendor
// @Tags LotteryPrizeVendor
// @Summary 删除LotteryPrizeVendor
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.LotteryPrizeVendor true "删除LotteryPrizeVendor"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /lotteryPrizeVendor/deleteLotteryPrizeVendor [delete]
func (lotteryPrizeVendorApi *LotteryPrizeVendorApi) DeleteLotteryPrizeVendor(c *gin.Context) {
	var lotteryPrizeVendor wclClassic.LotteryPrizeVendor
	err := c.ShouldBindJSON(&lotteryPrizeVendor)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := lotteryPrizeVendorService.DeleteLotteryPrizeVendor(lotteryPrizeVendor); err != nil {
        global.GVA_LOG.Error("删除失败!", zap.Error(err))
		response.FailWithMessage("删除失败", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// DeleteLotteryPrizeVendorByIds 批量删除LotteryPrizeVendor
// @Tags LotteryPrizeVendor
// @Summary 批量删除LotteryPrizeVendor
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除LotteryPrizeVendor"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"批量删除成功"}"
// @Router /lotteryPrizeVendor/deleteLotteryPrizeVendorByIds [delete]
func (lotteryPrizeVendorApi *LotteryPrizeVendorApi) DeleteLotteryPrizeVendorByIds(c *gin.Context) {
	var IDS request.IdsReq
    err := c.ShouldBindJSON(&IDS)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := lotteryPrizeVendorService.DeleteLotteryPrizeVendorByIds(IDS); err != nil {
        global.GVA_LOG.Error("批量删除失败!", zap.Error(err))
		response.FailWithMessage("批量删除失败", c)
	} else {
		response.OkWithMessage("批量删除成功", c)
	}
}

// UpdateLotteryPrizeVendor 更新LotteryPrizeVendor
// @Tags LotteryPrizeVendor
// @Summary 更新LotteryPrizeVendor
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.LotteryPrizeVendor true "更新LotteryPrizeVendor"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /lotteryPrizeVendor/updateLotteryPrizeVendor [put]
func (lotteryPrizeVendorApi *LotteryPrizeVendorApi) UpdateLotteryPrizeVendor(c *gin.Context) {
	var lotteryPrizeVendor wclClassic.LotteryPrizeVendor
	err := c.ShouldBindJSON(&lotteryPrizeVendor)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
      verify := utils.Rules{
          "Name":{utils.NotEmpty()},
      }
    if err := utils.Verify(lotteryPrizeVendor, verify); err != nil {
      	response.FailWithMessage(err.Error(), c)
      	return
     }
	if err := lotteryPrizeVendorService.UpdateLotteryPrizeVendor(lotteryPrizeVendor); err != nil {
        global.GVA_LOG.Error("更新失败!", zap.Error(err))
		response.FailWithMessage("更新失败", c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// FindLotteryPrizeVendor 用id查询LotteryPrizeVendor
// @Tags LotteryPrizeVendor
// @Summary 用id查询LotteryPrizeVendor
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query wclClassic.LotteryPrizeVendor true "用id查询LotteryPrizeVendor"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /lotteryPrizeVendor/findLotteryPrizeVendor [get]
func (lotteryPrizeVendorApi *LotteryPrizeVendorApi) FindLotteryPrizeVendor(c *gin.Context) {
	var lotteryPrizeVendor wclClassic.LotteryPrizeVendor
	err := c.ShouldBindQuery(&lotteryPrizeVendor)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if relotteryPrizeVendor, err := lotteryPrizeVendorService.GetLotteryPrizeVendor(lotteryPrizeVendor.ID); err != nil {
        global.GVA_LOG.Error("查询失败!", zap.Error(err))
		response.FailWithMessage("查询失败", c)
	} else {
		response.OkWithData(gin.H{"relotteryPrizeVendor": relotteryPrizeVendor}, c)
	}
}

// GetLotteryPrizeVendorList 分页获取LotteryPrizeVendor列表
// @Tags LotteryPrizeVendor
// @Summary 分页获取LotteryPrizeVendor列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query wclClassicReq.LotteryPrizeVendorSearch true "分页获取LotteryPrizeVendor列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /lotteryPrizeVendor/getLotteryPrizeVendorList [get]
func (lotteryPrizeVendorApi *LotteryPrizeVendorApi) GetLotteryPrizeVendorList(c *gin.Context) {
	var pageInfo wclClassicReq.LotteryPrizeVendorSearch
	err := c.ShouldBindQuery(&pageInfo)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if list, total, err := lotteryPrizeVendorService.GetLotteryPrizeVendorInfoList(pageInfo); err != nil {
	    global.GVA_LOG.Error("获取失败!", zap.Error(err))
        response.FailWithMessage("获取失败", c)
    } else {
        response.OkWithDetailed(response.PageResult{
            List:     list,
            Total:    total,
            Page:     pageInfo.Page,
            PageSize: pageInfo.PageSize,
        }, "获取成功", c)
    }
}
