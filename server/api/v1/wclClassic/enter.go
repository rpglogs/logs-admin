package wclClassic

type ApiGroup struct {
	AccountApi
	ArticleApi
	ArticleCategoryApi
	BannerApi
	ConfigApi
	StreamsApi
	LotteryActivityApi
	LotteryPrizeVendorApi
	LotteryPrizeApi
	LotteryActivityWinRecordApi
	CdkeyApi
	GoodsApi
	CharacterBackgroundsApi
	MilestoneReportApi
	MilestoneActivityApi
}
