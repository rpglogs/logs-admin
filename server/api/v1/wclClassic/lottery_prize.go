package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
    "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
    "github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
    wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
    "github.com/flipped-aurora/gin-vue-admin/server/model/common/response"
    "github.com/flipped-aurora/gin-vue-admin/server/service"
    "github.com/gin-gonic/gin"
    "go.uber.org/zap"
)

type LotteryPrizeApi struct {
}

var lotteryPrizeService = service.ServiceGroupApp.WclClassicServiceGroup.LotteryPrizeService


// CreateLotteryPrize 创建LotteryPrize
// @Tags LotteryPrize
// @Summary 创建LotteryPrize
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.LotteryPrize true "创建LotteryPrize"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /lotteryPrize/createLotteryPrize [post]
func (lotteryPrizeApi *LotteryPrizeApi) CreateLotteryPrize(c *gin.Context) {
	var lotteryPrize wclClassic.LotteryPrize
	err := c.ShouldBindJSON(&lotteryPrize)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := lotteryPrizeService.CreateLotteryPrize(&lotteryPrize); err != nil {
        global.GVA_LOG.Error("创建失败!", zap.Error(err))
		response.FailWithMessage("创建失败", c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// DeleteLotteryPrize 删除LotteryPrize
// @Tags LotteryPrize
// @Summary 删除LotteryPrize
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.LotteryPrize true "删除LotteryPrize"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /lotteryPrize/deleteLotteryPrize [delete]
func (lotteryPrizeApi *LotteryPrizeApi) DeleteLotteryPrize(c *gin.Context) {
	var lotteryPrize wclClassic.LotteryPrize
	err := c.ShouldBindJSON(&lotteryPrize)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := lotteryPrizeService.DeleteLotteryPrize(lotteryPrize); err != nil {
        global.GVA_LOG.Error("删除失败!", zap.Error(err))
		response.FailWithMessage("删除失败", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// DeleteLotteryPrizeByIds 批量删除LotteryPrize
// @Tags LotteryPrize
// @Summary 批量删除LotteryPrize
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除LotteryPrize"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"批量删除成功"}"
// @Router /lotteryPrize/deleteLotteryPrizeByIds [delete]
func (lotteryPrizeApi *LotteryPrizeApi) DeleteLotteryPrizeByIds(c *gin.Context) {
	var IDS request.IdsReq
    err := c.ShouldBindJSON(&IDS)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := lotteryPrizeService.DeleteLotteryPrizeByIds(IDS); err != nil {
        global.GVA_LOG.Error("批量删除失败!", zap.Error(err))
		response.FailWithMessage("批量删除失败", c)
	} else {
		response.OkWithMessage("批量删除成功", c)
	}
}

// UpdateLotteryPrize 更新LotteryPrize
// @Tags LotteryPrize
// @Summary 更新LotteryPrize
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.LotteryPrize true "更新LotteryPrize"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /lotteryPrize/updateLotteryPrize [put]
func (lotteryPrizeApi *LotteryPrizeApi) UpdateLotteryPrize(c *gin.Context) {
	var lotteryPrize wclClassic.LotteryPrize
	err := c.ShouldBindJSON(&lotteryPrize)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := lotteryPrizeService.UpdateLotteryPrize(lotteryPrize); err != nil {
        global.GVA_LOG.Error("更新失败!", zap.Error(err))
		response.FailWithMessage("更新失败", c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// FindLotteryPrize 用id查询LotteryPrize
// @Tags LotteryPrize
// @Summary 用id查询LotteryPrize
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query wclClassic.LotteryPrize true "用id查询LotteryPrize"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /lotteryPrize/findLotteryPrize [get]
func (lotteryPrizeApi *LotteryPrizeApi) FindLotteryPrize(c *gin.Context) {
	var lotteryPrize wclClassic.LotteryPrize
	err := c.ShouldBindQuery(&lotteryPrize)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if relotteryPrize, err := lotteryPrizeService.GetLotteryPrize(lotteryPrize.ID); err != nil {
        global.GVA_LOG.Error("查询失败!", zap.Error(err))
		response.FailWithMessage("查询失败", c)
	} else {
		response.OkWithData(gin.H{"relotteryPrize": relotteryPrize}, c)
	}
}

// GetLotteryPrizeList 分页获取LotteryPrize列表
// @Tags LotteryPrize
// @Summary 分页获取LotteryPrize列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query wclClassicReq.LotteryPrizeSearch true "分页获取LotteryPrize列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /lotteryPrize/getLotteryPrizeList [get]
func (lotteryPrizeApi *LotteryPrizeApi) GetLotteryPrizeList(c *gin.Context) {
	var pageInfo wclClassicReq.LotteryPrizeSearch
	err := c.ShouldBindQuery(&pageInfo)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if list, total, err := lotteryPrizeService.GetLotteryPrizeInfoList(pageInfo); err != nil {
	    global.GVA_LOG.Error("获取失败!", zap.Error(err))
        response.FailWithMessage("获取失败", c)
    } else {
        response.OkWithDetailed(response.PageResult{
            List:     list,
            Total:    total,
            Page:     pageInfo.Page,
            PageSize: pageInfo.PageSize,
        }, "获取成功", c)
    }
}
