package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/response"
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
	"github.com/flipped-aurora/gin-vue-admin/server/service"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

type StreamsApi struct {
}

var streamsService = service.ServiceGroupApp.WclClassicServiceGroup.StreamsService

// CreateStreams 创建Streams
// @Tags Streams
// @Summary 创建Streams
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.Streams true "创建Streams"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /streams/createStreams [post]
func (streamsApi *StreamsApi) CreateStreams(c *gin.Context) {
	var game = c.GetHeader("X-Game")
	var streams wclClassic.Streams
	err := c.ShouldBindJSON(&streams)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := streamsService.CreateStreams(&streams, game); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Error(err))
		response.FailWithMessage("创建失败", c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// DeleteStreams 删除Streams
// @Tags Streams
// @Summary 删除Streams
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.Streams true "删除Streams"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /streams/deleteStreams [delete]
func (streamsApi *StreamsApi) DeleteStreams(c *gin.Context) {
	var game = c.GetHeader("X-Game")
	var streams wclClassic.Streams
	err := c.ShouldBindJSON(&streams)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := streamsService.DeleteStreams(streams, game); err != nil {
		global.GVA_LOG.Error("删除失败!", zap.Error(err))
		response.FailWithMessage("删除失败", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// DeleteStreamsByIds 批量删除Streams
// @Tags Streams
// @Summary 批量删除Streams
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除Streams"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"批量删除成功"}"
// @Router /streams/deleteStreamsByIds [delete]
func (streamsApi *StreamsApi) DeleteStreamsByIds(c *gin.Context) {
	var game = c.GetHeader("X-Game")
	var IDS request.IdsReq
	err := c.ShouldBindJSON(&IDS)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := streamsService.DeleteStreamsByIds(IDS, game); err != nil {
		global.GVA_LOG.Error("批量删除失败!", zap.Error(err))
		response.FailWithMessage("批量删除失败", c)
	} else {
		response.OkWithMessage("批量删除成功", c)
	}
}

// UpdateStreams 更新Streams
// @Tags Streams
// @Summary 更新Streams
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.Streams true "更新Streams"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /streams/updateStreams [put]
func (streamsApi *StreamsApi) UpdateStreams(c *gin.Context) {
	var game = c.GetHeader("X-Game")
	var streams wclClassic.Streams
	err := c.ShouldBindJSON(&streams)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := streamsService.UpdateStreams(streams, game); err != nil {
		global.GVA_LOG.Error("更新失败!", zap.Error(err))
		response.FailWithMessage("更新失败", c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// FindStreams 用id查询Streams
// @Tags Streams
// @Summary 用id查询Streams
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query wclClassic.Streams true "用id查询Streams"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /streams/findStreams [get]
func (streamsApi *StreamsApi) FindStreams(c *gin.Context) {
	var game = c.GetHeader("X-Game")
	var streams wclClassic.Streams
	err := c.ShouldBindQuery(&streams)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if restreams, err := streamsService.GetStreams(streams.ID, game); err != nil {
		global.GVA_LOG.Error("查询失败!", zap.Error(err))
		response.FailWithMessage("查询失败", c)
	} else {
		response.OkWithData(gin.H{"restreams": restreams}, c)
	}
}

// GetStreamsList 分页获取Streams列表
// @Tags Streams
// @Summary 分页获取Streams列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query wclClassicReq.StreamsSearch true "分页获取Streams列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /streams/getStreamsList [get]
func (streamsApi *StreamsApi) GetStreamsList(c *gin.Context) {
	var game = c.GetHeader("X-Game")
	var pageInfo wclClassicReq.StreamsSearch
	err := c.ShouldBindQuery(&pageInfo)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if list, total, err := streamsService.GetStreamsInfoList(pageInfo, game); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Error(err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}
