package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
    "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
    "github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
    wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
    "github.com/flipped-aurora/gin-vue-admin/server/model/common/response"
    "github.com/flipped-aurora/gin-vue-admin/server/service"
    "github.com/gin-gonic/gin"
    "go.uber.org/zap"
)

type MilestoneActivityApi struct {
}

var milestoneActivityService = service.ServiceGroupApp.WclClassicServiceGroup.MilestoneActivityService


// CreateMilestoneActivity 创建MilestoneActivity
// @Tags MilestoneActivity
// @Summary 创建MilestoneActivity
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.MilestoneActivity true "创建MilestoneActivity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /milestoneActivity/createMilestoneActivity [post]
func (milestoneActivityApi *MilestoneActivityApi) CreateMilestoneActivity(c *gin.Context) {
	var milestoneActivity wclClassic.MilestoneActivity
	err := c.ShouldBindJSON(&milestoneActivity)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := milestoneActivityService.CreateMilestoneActivity(&milestoneActivity); err != nil {
        global.GVA_LOG.Error("创建失败!", zap.Error(err))
		response.FailWithMessage("创建失败", c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// DeleteMilestoneActivity 删除MilestoneActivity
// @Tags MilestoneActivity
// @Summary 删除MilestoneActivity
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.MilestoneActivity true "删除MilestoneActivity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /milestoneActivity/deleteMilestoneActivity [delete]
func (milestoneActivityApi *MilestoneActivityApi) DeleteMilestoneActivity(c *gin.Context) {
	var milestoneActivity wclClassic.MilestoneActivity
	err := c.ShouldBindJSON(&milestoneActivity)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := milestoneActivityService.DeleteMilestoneActivity(milestoneActivity); err != nil {
        global.GVA_LOG.Error("删除失败!", zap.Error(err))
		response.FailWithMessage("删除失败", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// DeleteMilestoneActivityByIds 批量删除MilestoneActivity
// @Tags MilestoneActivity
// @Summary 批量删除MilestoneActivity
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除MilestoneActivity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"批量删除成功"}"
// @Router /milestoneActivity/deleteMilestoneActivityByIds [delete]
func (milestoneActivityApi *MilestoneActivityApi) DeleteMilestoneActivityByIds(c *gin.Context) {
	var IDS request.IdsReq
    err := c.ShouldBindJSON(&IDS)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := milestoneActivityService.DeleteMilestoneActivityByIds(IDS); err != nil {
        global.GVA_LOG.Error("批量删除失败!", zap.Error(err))
		response.FailWithMessage("批量删除失败", c)
	} else {
		response.OkWithMessage("批量删除成功", c)
	}
}

// UpdateMilestoneActivity 更新MilestoneActivity
// @Tags MilestoneActivity
// @Summary 更新MilestoneActivity
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.MilestoneActivity true "更新MilestoneActivity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /milestoneActivity/updateMilestoneActivity [put]
func (milestoneActivityApi *MilestoneActivityApi) UpdateMilestoneActivity(c *gin.Context) {
	var milestoneActivity wclClassic.MilestoneActivity
	err := c.ShouldBindJSON(&milestoneActivity)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := milestoneActivityService.UpdateMilestoneActivity(milestoneActivity); err != nil {
        global.GVA_LOG.Error("更新失败!", zap.Error(err))
		response.FailWithMessage("更新失败", c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// FindMilestoneActivity 用id查询MilestoneActivity
// @Tags MilestoneActivity
// @Summary 用id查询MilestoneActivity
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query wclClassic.MilestoneActivity true "用id查询MilestoneActivity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /milestoneActivity/findMilestoneActivity [get]
func (milestoneActivityApi *MilestoneActivityApi) FindMilestoneActivity(c *gin.Context) {
	var milestoneActivity wclClassic.MilestoneActivity
	err := c.ShouldBindQuery(&milestoneActivity)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if remilestoneActivity, err := milestoneActivityService.GetMilestoneActivity(milestoneActivity.ID); err != nil {
        global.GVA_LOG.Error("查询失败!", zap.Error(err))
		response.FailWithMessage("查询失败", c)
	} else {
		response.OkWithData(gin.H{"remilestoneActivity": remilestoneActivity}, c)
	}
}

// GetMilestoneActivityList 分页获取MilestoneActivity列表
// @Tags MilestoneActivity
// @Summary 分页获取MilestoneActivity列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query wclClassicReq.MilestoneActivitySearch true "分页获取MilestoneActivity列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /milestoneActivity/getMilestoneActivityList [get]
func (milestoneActivityApi *MilestoneActivityApi) GetMilestoneActivityList(c *gin.Context) {
	var pageInfo wclClassicReq.MilestoneActivitySearch
	err := c.ShouldBindQuery(&pageInfo)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if list, total, err := milestoneActivityService.GetMilestoneActivityInfoList(pageInfo); err != nil {
	    global.GVA_LOG.Error("获取失败!", zap.Error(err))
        response.FailWithMessage("获取失败", c)
    } else {
        response.OkWithDetailed(response.PageResult{
            List:     list,
            Total:    total,
            Page:     pageInfo.Page,
            PageSize: pageInfo.PageSize,
        }, "获取成功", c)
    }
}
