package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
    "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
    "github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
    wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
    "github.com/flipped-aurora/gin-vue-admin/server/model/common/response"
    "github.com/flipped-aurora/gin-vue-admin/server/service"
    "github.com/gin-gonic/gin"
    "go.uber.org/zap"
)

type MilestoneReportApi struct {
}

var milestoneReportService = service.ServiceGroupApp.WclClassicServiceGroup.MilestoneReportService


// CreateMilestoneReport 创建MilestoneReport
// @Tags MilestoneReport
// @Summary 创建MilestoneReport
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.MilestoneReport true "创建MilestoneReport"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /milestoneReport/createMilestoneReport [post]
func (milestoneReportApi *MilestoneReportApi) CreateMilestoneReport(c *gin.Context) {
	var milestoneReport wclClassic.MilestoneReport
	err := c.ShouldBindJSON(&milestoneReport)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := milestoneReportService.CreateMilestoneReport(&milestoneReport); err != nil {
        global.GVA_LOG.Error("创建失败!", zap.Error(err))
		response.FailWithMessage("创建失败", c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// DeleteMilestoneReport 删除MilestoneReport
// @Tags MilestoneReport
// @Summary 删除MilestoneReport
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.MilestoneReport true "删除MilestoneReport"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /milestoneReport/deleteMilestoneReport [delete]
func (milestoneReportApi *MilestoneReportApi) DeleteMilestoneReport(c *gin.Context) {
	var milestoneReport wclClassic.MilestoneReport
	err := c.ShouldBindJSON(&milestoneReport)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := milestoneReportService.DeleteMilestoneReport(milestoneReport); err != nil {
        global.GVA_LOG.Error("删除失败!", zap.Error(err))
		response.FailWithMessage("删除失败", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// DeleteMilestoneReportByIds 批量删除MilestoneReport
// @Tags MilestoneReport
// @Summary 批量删除MilestoneReport
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除MilestoneReport"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"批量删除成功"}"
// @Router /milestoneReport/deleteMilestoneReportByIds [delete]
func (milestoneReportApi *MilestoneReportApi) DeleteMilestoneReportByIds(c *gin.Context) {
	var IDS request.IdsReq
    err := c.ShouldBindJSON(&IDS)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := milestoneReportService.DeleteMilestoneReportByIds(IDS); err != nil {
        global.GVA_LOG.Error("批量删除失败!", zap.Error(err))
		response.FailWithMessage("批量删除失败", c)
	} else {
		response.OkWithMessage("批量删除成功", c)
	}
}

// UpdateMilestoneReport 更新MilestoneReport
// @Tags MilestoneReport
// @Summary 更新MilestoneReport
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body wclClassic.MilestoneReport true "更新MilestoneReport"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /milestoneReport/updateMilestoneReport [put]
func (milestoneReportApi *MilestoneReportApi) UpdateMilestoneReport(c *gin.Context) {
	var milestoneReport wclClassic.MilestoneReport
	err := c.ShouldBindJSON(&milestoneReport)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := milestoneReportService.UpdateMilestoneReport(milestoneReport); err != nil {
        global.GVA_LOG.Error("更新失败!", zap.Error(err))
		response.FailWithMessage("更新失败", c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// FindMilestoneReport 用id查询MilestoneReport
// @Tags MilestoneReport
// @Summary 用id查询MilestoneReport
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query wclClassic.MilestoneReport true "用id查询MilestoneReport"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /milestoneReport/findMilestoneReport [get]
func (milestoneReportApi *MilestoneReportApi) FindMilestoneReport(c *gin.Context) {
	var milestoneReport wclClassic.MilestoneReport
	err := c.ShouldBindQuery(&milestoneReport)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if remilestoneReport, err := milestoneReportService.GetMilestoneReport(milestoneReport.ID); err != nil {
        global.GVA_LOG.Error("查询失败!", zap.Error(err))
		response.FailWithMessage("查询失败", c)
	} else {
		response.OkWithData(gin.H{"remilestoneReport": remilestoneReport}, c)
	}
}

// GetMilestoneReportList 分页获取MilestoneReport列表
// @Tags MilestoneReport
// @Summary 分页获取MilestoneReport列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query wclClassicReq.MilestoneReportSearch true "分页获取MilestoneReport列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /milestoneReport/getMilestoneReportList [get]
func (milestoneReportApi *MilestoneReportApi) GetMilestoneReportList(c *gin.Context) {
	var pageInfo wclClassicReq.MilestoneReportSearch
	err := c.ShouldBindQuery(&pageInfo)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if list, total, err := milestoneReportService.GetMilestoneReportInfoList(pageInfo); err != nil {
	    global.GVA_LOG.Error("获取失败!", zap.Error(err))
        response.FailWithMessage("获取失败", c)
    } else {
        response.OkWithDetailed(response.PageResult{
            List:     list,
            Total:    total,
            Page:     pageInfo.Page,
            PageSize: pageInfo.PageSize,
        }, "获取成功", c)
    }
}
