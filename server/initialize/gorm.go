package initialize

import (
	"os"

	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/example"
	"github.com/flipped-aurora/gin-vue-admin/server/model/system"

	wclClassicModel "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

func Gorm() *gorm.DB {
	switch global.GVA_CONFIG.System.DbType {
	case "mysql":
		return GormMysql()
	case "pgsql":
		return GormPgSql()
	case "oracle":
		return GormOracle()
	case "mssql":
		return GormMssql()
	default:
		return GormMysql()
	}
}

func RegisterTables() {
	wclClassic := global.GetGlobalDBByDBName("wclClassic")
	db := global.GVA_DB
	err := db.AutoMigrate(

		system.SysApi{},
		system.SysUser{},
		system.SysBaseMenu{},
		system.JwtBlacklist{},
		system.SysAuthority{},
		system.SysDictionary{},
		system.SysOperationRecord{},
		system.SysAutoCodeHistory{},
		system.SysDictionaryDetail{},
		system.SysBaseMenuParameter{},
		system.SysBaseMenuBtn{},
		system.SysAuthorityBtn{},
		system.SysAutoCode{},
		system.SysChatGptOption{},

		example.ExaFile{},
		example.ExaCustomer{},
		example.ExaFileChunk{},
		example.ExaFileUploadAndDownload{},
	)
	if err != nil {
		global.GVA_LOG.Error("register table failed", zap.Error(err))
		os.Exit(0)
	}
	global.GVA_LOG.Info("register table success")
	wclClassic.AutoMigrate(wclClassicModel.Account{}, wclClassicModel.Article{}, wclClassicModel.ArticleCategory{}, wclClassicModel.Banner{}, wclClassicModel.Config{}, wclClassicModel.Streams{}, wclClassicModel.LotteryActivity{}, wclClassicModel.LotteryPrizeVendor{}, wclClassicModel.LotteryPrize{}, wclClassicModel.LotteryActivityWinRecord{}, wclClassicModel.Cdkey{}, wclClassicModel.Goods{}, wclClassicModel.CharacterBackgrounds{}, wclClassicModel.MilestoneReport{}, wclClassicModel.MilestoneActivity{})
}
