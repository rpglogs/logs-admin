/*
 * @Author: Cheng Wei
 * @Date: 2023-06-21 14:14:08
 * @LastEditTime: 2023-08-22 12:46:23
 * @LastEditors: Cheng Wei
 * @Description:
 * @FilePath: \logs-admin\server\service\wclClassic\article_category.go
 * @GitHub: https://github.com/glegoo
 */
package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
)

type ArticleCategoryService struct {
}

// CreateArticleCategory 创建ArticleCategory记录
// Author [piexlmax](https://github.com/piexlmax)
func (articleCategoryService *ArticleCategoryService) CreateArticleCategory(articleCategory *wclClassic.ArticleCategory, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Create(articleCategory).Error
	return err
}

// DeleteArticleCategory 删除ArticleCategory记录
// Author [piexlmax](https://github.com/piexlmax)
func (articleCategoryService *ArticleCategoryService) DeleteArticleCategory(articleCategory wclClassic.ArticleCategory, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Delete(&articleCategory).Error
	return err
}

// DeleteArticleCategoryByIds 批量删除ArticleCategory记录
// Author [piexlmax](https://github.com/piexlmax)
func (articleCategoryService *ArticleCategoryService) DeleteArticleCategoryByIds(ids request.IdsReq, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Delete(&[]wclClassic.ArticleCategory{}, "id in ?", ids.Ids).Error
	return err
}

// UpdateArticleCategory 更新ArticleCategory记录
// Author [piexlmax](https://github.com/piexlmax)
func (articleCategoryService *ArticleCategoryService) UpdateArticleCategory(articleCategory wclClassic.ArticleCategory, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Save(&articleCategory).Error
	return err
}

// GetArticleCategory 根据id获取ArticleCategory记录
// Author [piexlmax](https://github.com/piexlmax)
func (articleCategoryService *ArticleCategoryService) GetArticleCategory(id uint, game string) (articleCategory wclClassic.ArticleCategory, err error) {
	err = global.MustGetGlobalDBByDBName(game).Where("id = ?", id).First(&articleCategory).Error
	return
}

// GetArticleCategoryInfoList 分页获取ArticleCategory记录
// Author [piexlmax](https://github.com/piexlmax)
func (articleCategoryService *ArticleCategoryService) GetArticleCategoryInfoList(info wclClassicReq.ArticleCategorySearch, game string) (list []wclClassic.ArticleCategory, total int64, err error) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	// 创建db
	db := global.MustGetGlobalDBByDBName(game).Model(&wclClassic.ArticleCategory{})
	var articleCategorys []wclClassic.ArticleCategory
	// 如果有条件搜索 下方会自动创建搜索语句
	if info.StartCreatedAt != nil && info.EndCreatedAt != nil {
		db = db.Where("created_at BETWEEN ? AND ?", info.StartCreatedAt, info.EndCreatedAt)
	}
	err = db.Count(&total).Error
	if err != nil {
		return
	}

	err = db.Limit(limit).Offset(offset).Find(&articleCategorys).Error
	return articleCategorys, total, err
}
