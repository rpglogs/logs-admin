/*
 * @Author: Cheng Wei
 * @Date: 2024-07-01 09:50:30
 * @LastEditTime: 2024-07-28 15:54:39
 * @LastEditors: Cheng Wei
 * @Description:
 * @FilePath: \logs-admin\server\service\wclClassic\lottery_activity.go
 * @GitHub: https://github.com/glegoo
 */
package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
	wclClassicResp "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/response"
)

type LotteryActivityService struct {
}

// CreateLotteryActivity 创建LotteryActivity记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryActivityService *LotteryActivityService) CreateLotteryActivity(lotteryActivity *wclClassic.LotteryActivity) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Create(lotteryActivity).Error
	return err
}

// DeleteLotteryActivity 删除LotteryActivity记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryActivityService *LotteryActivityService) DeleteLotteryActivity(lotteryActivity wclClassic.LotteryActivity) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Delete(&lotteryActivity).Error
	return err
}

// DeleteLotteryActivityByIds 批量删除LotteryActivity记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryActivityService *LotteryActivityService) DeleteLotteryActivityByIds(ids request.IdsReq) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Delete(&[]wclClassic.LotteryActivity{}, "id in ?", ids.Ids).Error
	return err
}

// UpdateLotteryActivity 更新LotteryActivity记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryActivityService *LotteryActivityService) UpdateLotteryActivity(lotteryActivity wclClassic.LotteryActivity) (err error) {
	tx := global.MustGetGlobalDBByDBName("wclClassic").Begin()
	err = tx.Save(&lotteryActivity).Error
	if err != nil {
		tx.Rollback()
		return err
	}

	// 更新prizes
	err = tx.Where("activity_id = ?", lotteryActivity.ID).Delete(&wclClassic.LotteryActivityPrize{}).Error
	// 创建prizes
	for _, prize := range *lotteryActivity.Prizes {
		prize.ActivityId = lotteryActivity.ID
		newPrize := &wclClassic.LotteryActivityPrize{
			ActivityId: prize.ActivityId,
			PrizeId:    prize.PrizeId,
			Num:        prize.Num,
		}
		err = tx.Create(newPrize).Error
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	tx.Commit()

	return err
}

// GetLotteryActivity 根据id获取LotteryActivity记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryActivityService *LotteryActivityService) GetLotteryActivity(id uint) (lotteryActivity wclClassic.LotteryActivity, err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Where("id = ?", id).Preload("Prizes").Preload("Prizes.Prize").First(&lotteryActivity).Error
	return
}

// GetLotteryActivityInfoList 分页获取LotteryActivity记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryActivityService *LotteryActivityService) GetLotteryActivityInfoList(info wclClassicReq.LotteryActivitySearch) (list []wclClassicResp.LotteryActivityInfo, total int64, err error) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	// 创建db
	db := global.MustGetGlobalDBByDBName("wclClassic").Model(&wclClassic.LotteryActivity{})
	var lotteryActivitys []wclClassicResp.LotteryActivityInfo
	// 如果有条件搜索 下方会自动创建搜索语句
	if info.StartCreatedAt != nil && info.EndCreatedAt != nil {
		db = db.Where("created_at BETWEEN ? AND ?", info.StartCreatedAt, info.EndCreatedAt)
	}
	if info.Name != "" {
		db = db.Where("name LIKE ?", "%"+info.Name+"%")
	}
	if info.ActivityType != nil {
		db = db.Where("activity_type = ?", info.ActivityType)
	}
	if info.VendorId != nil {
		db = db.Where("vendor_id = ?", info.VendorId)
	}
	if info.Status != nil {
		db = db.Where("status = ?", info.Status)
	}
	err = db.Count(&total).Error
	if err != nil {
		return
	}

	db = db.Select("lottery_activity.*, count(lottery_activity_entry.id) as entry_count")
	// join lottery_activity_entry 并计算参与人数
	db = db.Joins("left join lottery_activity_entry on lottery_activity.id = lottery_activity_entry.activity_id").Group("lottery_activity.id")

	err = db.Limit(limit).Offset(offset).Preload("Prizes").Preload("Prizes.Prize").Order("created_at desc").Find(&lotteryActivitys).Error
	return lotteryActivitys, total, err
}
