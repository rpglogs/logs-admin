package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
    wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
)

type MilestoneActivityService struct {
}

// CreateMilestoneActivity 创建MilestoneActivity记录
// Author [piexlmax](https://github.com/piexlmax)
func (milestoneActivityService *MilestoneActivityService) CreateMilestoneActivity(milestoneActivity *wclClassic.MilestoneActivity) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Create(milestoneActivity).Error
	return err
}

// DeleteMilestoneActivity 删除MilestoneActivity记录
// Author [piexlmax](https://github.com/piexlmax)
func (milestoneActivityService *MilestoneActivityService)DeleteMilestoneActivity(milestoneActivity wclClassic.MilestoneActivity) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Delete(&milestoneActivity).Error
	return err
}

// DeleteMilestoneActivityByIds 批量删除MilestoneActivity记录
// Author [piexlmax](https://github.com/piexlmax)
func (milestoneActivityService *MilestoneActivityService)DeleteMilestoneActivityByIds(ids request.IdsReq) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Delete(&[]wclClassic.MilestoneActivity{},"id in ?",ids.Ids).Error
	return err
}

// UpdateMilestoneActivity 更新MilestoneActivity记录
// Author [piexlmax](https://github.com/piexlmax)
func (milestoneActivityService *MilestoneActivityService)UpdateMilestoneActivity(milestoneActivity wclClassic.MilestoneActivity) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Save(&milestoneActivity).Error
	return err
}

// GetMilestoneActivity 根据id获取MilestoneActivity记录
// Author [piexlmax](https://github.com/piexlmax)
func (milestoneActivityService *MilestoneActivityService)GetMilestoneActivity(id uint) (milestoneActivity wclClassic.MilestoneActivity, err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Where("id = ?", id).First(&milestoneActivity).Error
	return
}

// GetMilestoneActivityInfoList 分页获取MilestoneActivity记录
// Author [piexlmax](https://github.com/piexlmax)
func (milestoneActivityService *MilestoneActivityService)GetMilestoneActivityInfoList(info wclClassicReq.MilestoneActivitySearch) (list []wclClassic.MilestoneActivity, total int64, err error) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
    // 创建db
	db := global.MustGetGlobalDBByDBName("wclClassic").Model(&wclClassic.MilestoneActivity{})
    var milestoneActivitys []wclClassic.MilestoneActivity
    // 如果有条件搜索 下方会自动创建搜索语句
    if info.StartCreatedAt !=nil && info.EndCreatedAt !=nil {
     db = db.Where("created_at BETWEEN ? AND ?", info.StartCreatedAt, info.EndCreatedAt)
    }
    if info.Uid != "" {
        db = db.Where("uid = ?",info.Uid)
    }
    if info.Title != "" {
        db = db.Where("title LIKE ?","%"+ info.Title+"%")
    }
    if info.GameType != "" {
        db = db.Where("game_type = ?",info.GameType)
    }
    if info.Region != "" {
        db = db.Where("region = ?",info.Region)
    }
    if info.Server != "" {
        db = db.Where("server = ?",info.Server)
    }
    if info.Faction != nil {
        db = db.Where("faction = ?",info.Faction)
    }
    if info.ShowContact != nil {
        db = db.Where("show_contact = ?",info.ShowContact)
    }
    if info.Size != nil {
        db = db.Where("size = ?",info.Size)
    }
    if info.IsPublic != nil {
        db = db.Where("is_public = ?",info.IsPublic)
    }
    if info.NotShare != nil {
        db = db.Where("not_share = ?",info.NotShare)
    }
	err = db.Count(&total).Error
	if err!=nil {
    	return
    }

	err = db.Limit(limit).Offset(offset).Find(&milestoneActivitys).Error
	return  milestoneActivitys, total, err
}
