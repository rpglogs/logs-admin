/*
 * @Author: Cheng Wei
 * @Date: 2023-06-13 13:28:44
 * @LastEditTime: 2023-08-22 12:29:54
 * @LastEditors: Cheng Wei
 * @Description:
 * @FilePath: \logs-admin\server\service\wclClassic\account.go
 * @GitHub: https://github.com/glegoo
 */
package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
)

type AccountService struct {
}

// CreateAccount 创建Account记录
// Author [piexlmax](https://github.com/piexlmax)
func (accountService *AccountService) CreateAccount(account *wclClassic.Account, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Create(account).Error
	return err
}

// DeleteAccount 删除Account记录
// Author [piexlmax](https://github.com/piexlmax)
func (accountService *AccountService) DeleteAccount(account wclClassic.Account, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Delete(&account).Error
	return err
}

// DeleteAccountByIds 批量删除Account记录
// Author [piexlmax](https://github.com/piexlmax)
func (accountService *AccountService) DeleteAccountByIds(ids request.IdsReq, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Delete(&[]wclClassic.Account{}, "uuid in ?", ids.Ids).Error
	return err
}

// UpdateAccount 更新Account记录
// Author [piexlmax](https://github.com/piexlmax)
func (accountService *AccountService) UpdateAccount(account wclClassic.Account, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Where("uuid = ?", account.Uuid).Save(&account).Error
	return err
}

// GetAccount 根据id获取Account记录
// Author [piexlmax](https://github.com/piexlmax)
func (accountService *AccountService) GetAccount(id string, game string) (account wclClassic.Account, err error) {
	err = global.MustGetGlobalDBByDBName(game).Where("uuid = ?", id).First(&account).Error
	return
}

// GetAccountInfoList 分页获取Account记录
// Author [piexlmax](https://github.com/piexlmax)
func (accountService *AccountService) GetAccountInfoList(info wclClassicReq.AccountSearch, game string) (list []wclClassic.Account, total int64, err error) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	// 创建db
	db := global.MustGetGlobalDBByDBName(game).Model(&wclClassic.Account{})
	var accounts []wclClassic.Account
	// 如果有条件搜索 下方会自动创建搜索语句
	if info.StartCreatedAt != nil && info.EndCreatedAt != nil {
		db = db.Where("created_at BETWEEN ? AND ?", info.StartCreatedAt, info.EndCreatedAt)
	}
	if info.StartLastLogin != nil && info.EndLastLogin != nil {
		db = db.Where("last_login BETWEEN ? AND ? ", info.StartLastLogin, info.EndLastLogin)
	}
	if info.Openid != "" {
		db = db.Where("openid = ?", info.Openid)
	}
	if info.IpAddress != "" {
		db = db.Where("ip_address = ?", info.IpAddress)
	}
	if info.Dkp != nil {
		db = db.Where("dkp > ?", info.Dkp)
	}
	if info.StartVipExpiryTime != nil && info.EndVipExpiryTime != nil {
		db = db.Where("vip_expiry_time BETWEEN ? AND ? ", info.StartVipExpiryTime, info.EndVipExpiryTime)
	}
	if info.Name != "" {
		db = db.Where("name LIKE ?", "%"+info.Name+"%")
	}
	err = db.Count(&total).Error
	if err != nil {
		return
	}

	err = db.Limit(limit).Offset(offset).Find(&accounts).Error
	return accounts, total, err
}
