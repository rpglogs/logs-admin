/*
 * @Author: Cheng Wei
 * @Date: 2023-07-19 09:07:10
 * @LastEditTime: 2023-08-22 12:37:40
 * @LastEditors: Cheng Wei
 * @Description:
 * @FilePath: \logs-admin\server\service\wclClassic\banner.go
 * @GitHub: https://github.com/glegoo
 */
package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
)

type BannerService struct {
}

// CreateBanner 创建Banner记录
// Author [piexlmax](https://github.com/piexlmax)
func (bannerService *BannerService) CreateBanner(banner *wclClassic.Banner, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Create(banner).Error
	return err
}

// DeleteBanner 删除Banner记录
// Author [piexlmax](https://github.com/piexlmax)
func (bannerService *BannerService) DeleteBanner(banner wclClassic.Banner, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Delete(&banner).Error
	return err
}

// DeleteBannerByIds 批量删除Banner记录
// Author [piexlmax](https://github.com/piexlmax)
func (bannerService *BannerService) DeleteBannerByIds(ids request.IdsReq, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Delete(&[]wclClassic.Banner{}, "id in ?", ids.Ids).Error
	return err
}

// UpdateBanner 更新Banner记录
// Author [piexlmax](https://github.com/piexlmax)
func (bannerService *BannerService) UpdateBanner(banner wclClassic.Banner, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Save(&banner).Error
	return err
}

// GetBanner 根据id获取Banner记录
// Author [piexlmax](https://github.com/piexlmax)
func (bannerService *BannerService) GetBanner(id uint, game string) (banner wclClassic.Banner, err error) {
	err = global.MustGetGlobalDBByDBName(game).Where("id = ?", id).First(&banner).Error
	return
}

// GetBannerInfoList 分页获取Banner记录
// Author [piexlmax](https://github.com/piexlmax)
func (bannerService *BannerService) GetBannerInfoList(info wclClassicReq.BannerSearch, game string) (list []wclClassic.Banner, total int64, err error) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	// 创建db
	db := global.MustGetGlobalDBByDBName(game).Model(&wclClassic.Banner{})
	var banners []wclClassic.Banner
	// 如果有条件搜索 下方会自动创建搜索语句
	if info.StartCreatedAt != nil && info.EndCreatedAt != nil {
		db = db.Where("created_at BETWEEN ? AND ?", info.StartCreatedAt, info.EndCreatedAt)
	}
	if info.Type != nil {
		db = db.Where("type = ?", info.Type)
	}
	err = db.Count(&total).Error
	if err != nil {
		return
	}

	err = db.Limit(limit).Offset(offset).Find(&banners).Error
	return banners, total, err
}
