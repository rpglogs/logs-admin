package wclClassic

type ServiceGroup struct {
	AccountService
	ArticleService
	ArticleCategoryService
	BannerService
	ConfigService
	StreamsService
	LotteryActivityService
	LotteryPrizeVendorService
	LotteryPrizeService
	LotteryActivityWinRecordService
	CdkeyService
	GoodsService
	CharacterBackgroundsService
	MilestoneReportService
	MilestoneActivityService
}
