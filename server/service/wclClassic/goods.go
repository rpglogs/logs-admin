package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
)

type GoodsService struct {
}

// CreateGoods 创建Goods记录
// Author [piexlmax](https://github.com/piexlmax)
func (goodsService *GoodsService) CreateGoods(goods *wclClassic.Goods, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Create(goods).Error
	return err
}

// DeleteGoods 删除Goods记录
// Author [piexlmax](https://github.com/piexlmax)
func (goodsService *GoodsService) DeleteGoods(goods wclClassic.Goods, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Delete(&goods).Error
	return err
}

// DeleteGoodsByIds 批量删除Goods记录
// Author [piexlmax](https://github.com/piexlmax)
func (goodsService *GoodsService) DeleteGoodsByIds(ids request.IdsReq, game string) (err error) {

	err = global.MustGetGlobalDBByDBName(game).Delete(&[]wclClassic.Goods{}, "id in ?", ids.Ids).Error
	return err
}

// UpdateGoods 更新Goods记录
// Author [piexlmax](https://github.com/piexlmax)
func (goodsService *GoodsService) UpdateGoods(goods wclClassic.Goods, game string) (err error) {

	err = global.MustGetGlobalDBByDBName(game).Save(&goods).Error
	return err
}

// GetGoods 根据id获取Goods记录
// Author [piexlmax](https://github.com/piexlmax)
func (goodsService *GoodsService) GetGoods(id uint, game string) (goods wclClassic.Goods, err error) {

	err = global.MustGetGlobalDBByDBName(game).Where("id = ?", id).First(&goods).Error
	return
}

// GetGoodsInfoList 分页获取Goods记录
// Author [piexlmax](https://github.com/piexlmax)
func (goodsService *GoodsService) GetGoodsInfoList(info wclClassicReq.GoodsSearch, game string) (list []wclClassic.Goods, total int64, err error) {

	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	// 创建db
	db := global.MustGetGlobalDBByDBName(game).Model(&wclClassic.Goods{})
	var goodss []wclClassic.Goods
	// 如果有条件搜索 下方会自动创建搜索语句
	if info.StartCreatedAt != nil && info.EndCreatedAt != nil {
		db = db.Where("created_at BETWEEN ? AND ?", info.StartCreatedAt, info.EndCreatedAt)
	}
	if info.Type != "" {
		db = db.Where("type = ?", info.Type)
	}
	if info.Description != "" {
		db = db.Where("description LIKE ?", "%"+info.Description+"%")
	}
	if info.Days != nil {
		db = db.Where("days = ?", info.Days)
	}
	err = db.Count(&total).Error
	if err != nil {
		return
	}

	err = db.Limit(limit).Offset(offset).Find(&goodss).Error
	return goodss, total, err
}
