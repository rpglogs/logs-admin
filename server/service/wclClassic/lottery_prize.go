package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
    wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
)

type LotteryPrizeService struct {
}

// CreateLotteryPrize 创建LotteryPrize记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryPrizeService *LotteryPrizeService) CreateLotteryPrize(lotteryPrize *wclClassic.LotteryPrize) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Create(lotteryPrize).Error
	return err
}

// DeleteLotteryPrize 删除LotteryPrize记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryPrizeService *LotteryPrizeService)DeleteLotteryPrize(lotteryPrize wclClassic.LotteryPrize) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Delete(&lotteryPrize).Error
	return err
}

// DeleteLotteryPrizeByIds 批量删除LotteryPrize记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryPrizeService *LotteryPrizeService)DeleteLotteryPrizeByIds(ids request.IdsReq) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Delete(&[]wclClassic.LotteryPrize{},"id in ?",ids.Ids).Error
	return err
}

// UpdateLotteryPrize 更新LotteryPrize记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryPrizeService *LotteryPrizeService)UpdateLotteryPrize(lotteryPrize wclClassic.LotteryPrize) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Save(&lotteryPrize).Error
	return err
}

// GetLotteryPrize 根据id获取LotteryPrize记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryPrizeService *LotteryPrizeService)GetLotteryPrize(id uint) (lotteryPrize wclClassic.LotteryPrize, err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Where("id = ?", id).First(&lotteryPrize).Error
	return
}

// GetLotteryPrizeInfoList 分页获取LotteryPrize记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryPrizeService *LotteryPrizeService)GetLotteryPrizeInfoList(info wclClassicReq.LotteryPrizeSearch) (list []wclClassic.LotteryPrize, total int64, err error) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
    // 创建db
	db := global.MustGetGlobalDBByDBName("wclClassic").Model(&wclClassic.LotteryPrize{})
    var lotteryPrizes []wclClassic.LotteryPrize
    // 如果有条件搜索 下方会自动创建搜索语句
    if info.StartCreatedAt !=nil && info.EndCreatedAt !=nil {
     db = db.Where("created_at BETWEEN ? AND ?", info.StartCreatedAt, info.EndCreatedAt)
    }
    if info.Name != "" {
        db = db.Where("name LIKE ?","%"+ info.Name+"%")
    }
    if info.VendorId != nil {
        db = db.Where("vendor_id = ?",info.VendorId)
    }
	err = db.Count(&total).Error
	if err!=nil {
    	return
    }

	err = db.Limit(limit).Offset(offset).Find(&lotteryPrizes).Error
	return  lotteryPrizes, total, err
}
