package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
    wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
)

type MilestoneReportService struct {
}

// CreateMilestoneReport 创建MilestoneReport记录
// Author [piexlmax](https://github.com/piexlmax)
func (milestoneReportService *MilestoneReportService) CreateMilestoneReport(milestoneReport *wclClassic.MilestoneReport) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Create(milestoneReport).Error
	return err
}

// DeleteMilestoneReport 删除MilestoneReport记录
// Author [piexlmax](https://github.com/piexlmax)
func (milestoneReportService *MilestoneReportService)DeleteMilestoneReport(milestoneReport wclClassic.MilestoneReport) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Delete(&milestoneReport).Error
	return err
}

// DeleteMilestoneReportByIds 批量删除MilestoneReport记录
// Author [piexlmax](https://github.com/piexlmax)
func (milestoneReportService *MilestoneReportService)DeleteMilestoneReportByIds(ids request.IdsReq) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Delete(&[]wclClassic.MilestoneReport{},"id in ?",ids.Ids).Error
	return err
}

// UpdateMilestoneReport 更新MilestoneReport记录
// Author [piexlmax](https://github.com/piexlmax)
func (milestoneReportService *MilestoneReportService)UpdateMilestoneReport(milestoneReport wclClassic.MilestoneReport) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Save(&milestoneReport).Error
	return err
}

// GetMilestoneReport 根据id获取MilestoneReport记录
// Author [piexlmax](https://github.com/piexlmax)
func (milestoneReportService *MilestoneReportService)GetMilestoneReport(id uint) (milestoneReport wclClassic.MilestoneReport, err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Where("id = ?", id).First(&milestoneReport).Error
	return
}

// GetMilestoneReportInfoList 分页获取MilestoneReport记录
// Author [piexlmax](https://github.com/piexlmax)
func (milestoneReportService *MilestoneReportService)GetMilestoneReportInfoList(info wclClassicReq.MilestoneReportSearch) (list []wclClassic.MilestoneReport, total int64, err error) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
    // 创建db
	db := global.MustGetGlobalDBByDBName("wclClassic").Model(&wclClassic.MilestoneReport{})
    var milestoneReports []wclClassic.MilestoneReport
    // 如果有条件搜索 下方会自动创建搜索语句
    if info.StartCreatedAt !=nil && info.EndCreatedAt !=nil {
     db = db.Where("created_at BETWEEN ? AND ?", info.StartCreatedAt, info.EndCreatedAt)
    }
    if info.ActivityId != nil {
        db = db.Where("activity_id = ?",info.ActivityId)
    }
    if info.ReportType != nil {
        db = db.Where("report_type = ?",info.ReportType)
    }
    if info.ReporterUid != "" {
        db = db.Where("reporter_uid = ?",info.ReporterUid)
    }
    if info.Status != nil {
        db = db.Where("status = ?",info.Status)
    }
	err = db.Count(&total).Error
	if err!=nil {
    	return
    }

	err = db.Limit(limit).Offset(offset).Find(&milestoneReports).Error
	return  milestoneReports, total, err
}
