package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
    wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
)

type CharacterBackgroundsService struct {
}

// CreateCharacterBackgrounds 创建CharacterBackgrounds记录
// Author [piexlmax](https://github.com/piexlmax)
func (characterBackgroundsService *CharacterBackgroundsService) CreateCharacterBackgrounds(characterBackgrounds *wclClassic.CharacterBackgrounds) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Create(characterBackgrounds).Error
	return err
}

// DeleteCharacterBackgrounds 删除CharacterBackgrounds记录
// Author [piexlmax](https://github.com/piexlmax)
func (characterBackgroundsService *CharacterBackgroundsService)DeleteCharacterBackgrounds(characterBackgrounds wclClassic.CharacterBackgrounds) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Delete(&characterBackgrounds).Error
	return err
}

// DeleteCharacterBackgroundsByIds 批量删除CharacterBackgrounds记录
// Author [piexlmax](https://github.com/piexlmax)
func (characterBackgroundsService *CharacterBackgroundsService)DeleteCharacterBackgroundsByIds(ids request.IdsReq) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Delete(&[]wclClassic.CharacterBackgrounds{},"id in ?",ids.Ids).Error
	return err
}

// UpdateCharacterBackgrounds 更新CharacterBackgrounds记录
// Author [piexlmax](https://github.com/piexlmax)
func (characterBackgroundsService *CharacterBackgroundsService)UpdateCharacterBackgrounds(characterBackgrounds wclClassic.CharacterBackgrounds) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Save(&characterBackgrounds).Error
	return err
}

// GetCharacterBackgrounds 根据id获取CharacterBackgrounds记录
// Author [piexlmax](https://github.com/piexlmax)
func (characterBackgroundsService *CharacterBackgroundsService)GetCharacterBackgrounds(id uint) (characterBackgrounds wclClassic.CharacterBackgrounds, err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Where("id = ?", id).First(&characterBackgrounds).Error
	return
}

// GetCharacterBackgroundsInfoList 分页获取CharacterBackgrounds记录
// Author [piexlmax](https://github.com/piexlmax)
func (characterBackgroundsService *CharacterBackgroundsService)GetCharacterBackgroundsInfoList(info wclClassicReq.CharacterBackgroundsSearch) (list []wclClassic.CharacterBackgrounds, total int64, err error) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
    // 创建db
	db := global.MustGetGlobalDBByDBName("wclClassic").Model(&wclClassic.CharacterBackgrounds{})
    var characterBackgroundss []wclClassic.CharacterBackgrounds
    // 如果有条件搜索 下方会自动创建搜索语句
    if info.StartCreatedAt !=nil && info.EndCreatedAt !=nil {
     db = db.Where("created_at BETWEEN ? AND ?", info.StartCreatedAt, info.EndCreatedAt)
    }
    if info.CharacterId != nil {
        db = db.Where("character_id = ?",info.CharacterId)
    }
    if info.Status != nil {
        db = db.Where("status = ?",info.Status)
    }
    if info.Uuid != "" {
        db = db.Where("uuid = ?",info.Uuid)
    }
    if info.WclId != nil {
        db = db.Where("wcl_id = ?",info.WclId)
    }
	err = db.Count(&total).Error
	if err!=nil {
    	return
    }

	err = db.Limit(limit).Offset(offset).Find(&characterBackgroundss).Error
	return  characterBackgroundss, total, err
}
