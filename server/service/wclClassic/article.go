package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
	"gorm.io/gorm"
)

type ArticleService struct {
}

// CreateArticle 创建Article记录
// Author [piexlmax](https://github.com/piexlmax)
func (articleService *ArticleService) CreateArticle(article *wclClassic.Article, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Create(article).Error
	return err
}

// DeleteArticle 删除Article记录
// Author [piexlmax](https://github.com/piexlmax)
func (articleService *ArticleService) DeleteArticle(article wclClassic.Article, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Transaction(func(tx *gorm.DB) error {
		if err := tx.Model(&wclClassic.Article{}).Where("id = ?", article.ID).Update("deleted_by", article.DeletedBy).Error; err != nil {
			return err
		}
		if err = tx.Delete(&article).Error; err != nil {
			return err
		}
		return nil
	})
	return err
}

// DeleteArticleByIds 批量删除Article记录
// Author [piexlmax](https://github.com/piexlmax)
func (articleService *ArticleService) DeleteArticleByIds(ids request.IdsReq, deleted_by uint, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Transaction(func(tx *gorm.DB) error {
		if err := tx.Model(&wclClassic.Article{}).Where("id in ?", ids.Ids).Update("deleted_by", deleted_by).Error; err != nil {
			return err
		}
		if err := tx.Where("id in ?", ids.Ids).Delete(&wclClassic.Article{}).Error; err != nil {
			return err
		}
		return nil
	})
	return err
}

// UpdateArticle 更新Article记录
// Author [piexlmax](https://github.com/piexlmax)
func (articleService *ArticleService) UpdateArticle(article wclClassic.Article, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Save(&article).Error
	return err
}

// GetArticle 根据id获取Article记录
// Author [piexlmax](https://github.com/piexlmax)
func (articleService *ArticleService) GetArticle(id uint, game string) (article wclClassic.Article, err error) {
	err = global.MustGetGlobalDBByDBName(game).Where("id = ?", id).First(&article).Error
	return
}

// GetArticleInfoList 分页获取Article记录
// Author [piexlmax](https://github.com/piexlmax)
func (articleService *ArticleService) GetArticleInfoList(info wclClassicReq.ArticleSearch, game string) (list []wclClassic.Article, total int64, err error) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	// 创建db
	db := global.MustGetGlobalDBByDBName(game).Model(&wclClassic.Article{})
	var articles []wclClassic.Article
	// 如果有条件搜索 下方会自动创建搜索语句
	if info.StartCreatedAt != nil && info.EndCreatedAt != nil {
		db = db.Where("created_at BETWEEN ? AND ?", info.StartCreatedAt, info.EndCreatedAt)
	}
	if info.Title != "" {
		db = db.Where("title LIKE ?", "%"+info.Title+"%")
	}
	if info.Category != nil {
		db = db.Where("category = ?", info.Category)
	}
	err = db.Count(&total).Error
	if err != nil {
		return
	}

	err = db.Limit(limit).Offset(offset).Find(&articles).Error
	return articles, total, err
}
