/*
 * @Author: Cheng Wei
 * @Date: 2024-07-22 09:14:43
 * @LastEditTime: 2024-07-22 21:34:39
 * @LastEditors: Cheng Wei
 * @Description:
 * @FilePath: \logs-admin\server\service\wclClassic\lottery_activity_win_record.go
 * @GitHub: https://github.com/glegoo
 */
package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
)

type LotteryActivityWinRecordService struct {
}

// CreateLotteryActivityWinRecord 创建LotteryActivityWinRecord记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryActivityWinRecordService *LotteryActivityWinRecordService) CreateLotteryActivityWinRecord(lotteryActivityWinRecord *wclClassic.LotteryActivityWinRecord) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Create(lotteryActivityWinRecord).Error
	return err
}

// DeleteLotteryActivityWinRecord 删除LotteryActivityWinRecord记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryActivityWinRecordService *LotteryActivityWinRecordService) DeleteLotteryActivityWinRecord(lotteryActivityWinRecord wclClassic.LotteryActivityWinRecord) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Delete(&lotteryActivityWinRecord).Error
	return err
}

// DeleteLotteryActivityWinRecordByIds 批量删除LotteryActivityWinRecord记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryActivityWinRecordService *LotteryActivityWinRecordService) DeleteLotteryActivityWinRecordByIds(ids request.IdsReq) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Delete(&[]wclClassic.LotteryActivityWinRecord{}, "id in ?", ids.Ids).Error
	return err
}

// UpdateLotteryActivityWinRecord 更新LotteryActivityWinRecord记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryActivityWinRecordService *LotteryActivityWinRecordService) UpdateLotteryActivityWinRecord(lotteryActivityWinRecord wclClassic.LotteryActivityWinRecord) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Save(&lotteryActivityWinRecord).Error
	return err
}

// GetLotteryActivityWinRecord 根据id获取LotteryActivityWinRecord记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryActivityWinRecordService *LotteryActivityWinRecordService) GetLotteryActivityWinRecord(id uint) (lotteryActivityWinRecord wclClassic.LotteryActivityWinRecord, err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Preload("Prize").Preload("Vendor").Preload("User").Preload("Address").Where("id = ?", id).First(&lotteryActivityWinRecord).Error
	return
}

// GetLotteryActivityWinRecordInfoList 分页获取LotteryActivityWinRecord记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryActivityWinRecordService *LotteryActivityWinRecordService) GetLotteryActivityWinRecordInfoList(info wclClassicReq.LotteryActivityWinRecordSearch) (list []wclClassic.LotteryActivityWinRecord, total int64, err error) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	// 创建db
	db := global.MustGetGlobalDBByDBName("wclClassic").Model(&wclClassic.LotteryActivityWinRecord{})
	var lotteryActivityWinRecords []wclClassic.LotteryActivityWinRecord
	// 如果有条件搜索 下方会自动创建搜索语句
	if info.StartCreatedAt != nil && info.EndCreatedAt != nil {
		db = db.Where("created_at BETWEEN ? AND ?", info.StartCreatedAt, info.EndCreatedAt)
	}
	if info.ID != 0 {
		db = db.Where("id = ?", info.ID)
	}
	if info.ActivityId != nil {
		db = db.Where("activity_id = ?", info.ActivityId)
	}
	if info.Status != nil {
		db = db.Where("status = ?", info.Status)
	}
	if info.PrizeId != nil {
		db = db.Where("prize_id = ?", info.PrizeId)
	}
	if info.VendorId != nil {
		db = db.Where("vendor_id = ?", info.VendorId)
	}
	if info.Uid != "" {
		db = db.Where("uid = ?", info.Uid)
	}
	if info.ExpressCompany != "" {
		db = db.Where("express_company = ?", info.ExpressCompany)
	}
	if info.ExpressNumber != "" {
		db = db.Where("express_number = ?", info.ExpressNumber)
	}
	if info.PrizeOrderId != "" {
		db = db.Where("prize_order_id = ?", info.PrizeOrderId)
	}
	if info.Prize.Type != 0 {
		db = db.Joins("left join lottery_prize on lottery_activity_win_record.prize_id = lottery_prize.id").Where("lottery_prize.type = ?", info.Prize.Type)
	}
	err = db.Count(&total).Error

	if err != nil {
		return
	}

	err = db.Limit(limit).Offset(offset).Preload("Prize").Preload("Vendor").Preload("User").Preload("Address").Order("created_at desc").Find(&lotteryActivityWinRecords).Error
	return lotteryActivityWinRecords, total, err
}
