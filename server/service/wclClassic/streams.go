package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
)

type StreamsService struct {
}

// CreateStreams 创建Streams记录
// Author [piexlmax](https://github.com/piexlmax)
func (streamsService *StreamsService) CreateStreams(streams *wclClassic.Streams, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Create(streams).Error
	return err
}

// DeleteStreams 删除Streams记录
// Author [piexlmax](https://github.com/piexlmax)
func (streamsService *StreamsService) DeleteStreams(streams wclClassic.Streams, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Delete(&streams).Error
	return err
}

// DeleteStreamsByIds 批量删除Streams记录
// Author [piexlmax](https://github.com/piexlmax)
func (streamsService *StreamsService) DeleteStreamsByIds(ids request.IdsReq, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Delete(&[]wclClassic.Streams{}, "id in ?", ids.Ids).Error
	return err
}

// UpdateStreams 更新Streams记录
// Author [piexlmax](https://github.com/piexlmax)
func (streamsService *StreamsService) UpdateStreams(streams wclClassic.Streams, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Save(&streams).Error
	return err
}

// GetStreams 根据id获取Streams记录
// Author [piexlmax](https://github.com/piexlmax)
func (streamsService *StreamsService) GetStreams(id uint, game string) (streams wclClassic.Streams, err error) {
	err = global.MustGetGlobalDBByDBName(game).Where("id = ?", id).First(&streams).Error
	return
}

// GetStreamsInfoList 分页获取Streams记录
// Author [piexlmax](https://github.com/piexlmax)
func (streamsService *StreamsService) GetStreamsInfoList(info wclClassicReq.StreamsSearch, game string) (list []wclClassic.Streams, total int64, err error) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	// 创建db
	db := global.MustGetGlobalDBByDBName(game).Model(&wclClassic.Streams{})
	var streamss []wclClassic.Streams
	// 如果有条件搜索 下方会自动创建搜索语句
	if info.StartCreatedAt != nil && info.EndCreatedAt != nil {
		db = db.Where("created_at BETWEEN ? AND ?", info.StartCreatedAt, info.EndCreatedAt)
	}
	if info.AccountId != "" {
		db = db.Where("account_id = ?", info.AccountId)
	}
	if info.Type != nil {
		db = db.Where("type = ?", info.Type)
	}
	if info.RoomId != "" {
		db = db.Where("room_id = ?", info.RoomId)
	}
	if info.CharacterId != nil {
		db = db.Where("character_id = ?", info.CharacterId)
	}
	if info.ClassId != nil {
		db = db.Where("class_id = ?", info.ClassId)
	}
	err = db.Count(&total).Error
	if err != nil {
		return
	}

	err = db.Limit(limit).Offset(offset).Find(&streamss).Error
	return streamss, total, err
}
