package wclClassic

import (
	"math/rand"

	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
)

type CdkeyService struct {
}

// CreateCdkey 创建Cdkey记录
// Author [piexlmax](https://github.com/piexlmax)
func (cdkeyService *CdkeyService) CreateCdkey(cdkey *wclClassic.Cdkey, game string) (err error) {
	cdkey.Code = cdkeyService.GenearateCode()
	err = global.MustGetGlobalDBByDBName(game).Create(cdkey).Error
	return err
}

// 生成32位随机大写字母+数字字符串，不包含0,O,1,I，随机种子为time.Now().Unix()，长度为20
func (cdkeyService *CdkeyService) GenearateCode() string {
	strList := []string{"A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "2", "3", "4", "5", "6", "7", "8", "9"}
	code := ""
	for i := 0; i < 20; i++ {
		code += strList[rand.Intn(len(strList))]
	}
	return code
}

func (cdkeyService *CdkeyService) CreateCdkeys(cdkeys []wclClassic.Cdkey, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Create(&cdkeys).Error
	return err
}

func (cdkeyService *CdkeyService) BatchCreateCdkeys(request *wclClassicReq.BatchCreateCdkeys, game string) (list []wclClassic.Cdkey, err error) {
	list = make([]wclClassic.Cdkey, 0)
	tx := global.MustGetGlobalDBByDBName(game).Begin()
	for i := 0; i < request.Num; i++ {
		cdkey := wclClassic.Cdkey{
			PrizeId: request.PrizeId,
			Status:  1,
			Code:    cdkeyService.GenearateCode(),
			Remark:  request.Remark,
		}
		err = tx.Create(&cdkey).Error
		if err != nil {
			tx.Rollback()
			return
		}
		list = append(list, cdkey)
	}
	tx.Commit()
	return
}

func (cdkeyService *CdkeyService) DeleteCdkey(cdkey wclClassic.Cdkey, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Delete(&cdkey).Error
	return err
}

func (cdkeyService *CdkeyService) DeleteCdkeyByIds(ids request.IdsReq, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Delete(&[]wclClassic.Cdkey{}, "id in ?", ids.Ids).Error
	return err
}

func (cdkeyService *CdkeyService) UpdateCdkey(cdkey wclClassic.Cdkey, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Save(&cdkey).Error
	return err
}

func (cdkeyService *CdkeyService) GetCdkey(id uint, game string) (cdkey wclClassic.Cdkey, err error) {
	err = global.MustGetGlobalDBByDBName(game).Preload("User").Preload("Prize").Where("id = ?", id).First(&cdkey).Error
	return
}

func (cdkeyService *CdkeyService) GetCdkeyInfoList(info wclClassicReq.CdkeySearch, game string) (list []wclClassic.Cdkey, total int64, err error) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	db := global.MustGetGlobalDBByDBName(game).Model(&wclClassic.Cdkey{}).Preload("User").Preload("Prize")
	var cdkeys []wclClassic.Cdkey
	if info.StartCreatedAt != nil && info.EndCreatedAt != nil {
		db = db.Where("created_at BETWEEN ? AND ?", info.StartCreatedAt, info.EndCreatedAt)
	}
	if info.Code != "" {
		db = db.Where("code = ?", info.Code)
	}
	if info.Status != 0 {
		db = db.Where("status = ?", info.Status)
	}
	if info.Uid != nil {
		db = db.Where("uid = ?", info.Uid)
	}
	if info.PrizeId != 0 {
		db = db.Where("prize_id = ?", info.PrizeId)
	}
	if info.Remark != "" {
		db = db.Where("remark LIKE ?", "%"+info.Remark+"%")
	}
	err = db.Count(&total).Error
	if err != nil {
		return
	}

	err = db.Limit(limit).Offset(offset).Find(&cdkeys).Error
	return cdkeys, total, err
}
