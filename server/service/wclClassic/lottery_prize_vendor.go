package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
    wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
)

type LotteryPrizeVendorService struct {
}

// CreateLotteryPrizeVendor 创建LotteryPrizeVendor记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryPrizeVendorService *LotteryPrizeVendorService) CreateLotteryPrizeVendor(lotteryPrizeVendor *wclClassic.LotteryPrizeVendor) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Create(lotteryPrizeVendor).Error
	return err
}

// DeleteLotteryPrizeVendor 删除LotteryPrizeVendor记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryPrizeVendorService *LotteryPrizeVendorService)DeleteLotteryPrizeVendor(lotteryPrizeVendor wclClassic.LotteryPrizeVendor) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Delete(&lotteryPrizeVendor).Error
	return err
}

// DeleteLotteryPrizeVendorByIds 批量删除LotteryPrizeVendor记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryPrizeVendorService *LotteryPrizeVendorService)DeleteLotteryPrizeVendorByIds(ids request.IdsReq) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Delete(&[]wclClassic.LotteryPrizeVendor{},"id in ?",ids.Ids).Error
	return err
}

// UpdateLotteryPrizeVendor 更新LotteryPrizeVendor记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryPrizeVendorService *LotteryPrizeVendorService)UpdateLotteryPrizeVendor(lotteryPrizeVendor wclClassic.LotteryPrizeVendor) (err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Save(&lotteryPrizeVendor).Error
	return err
}

// GetLotteryPrizeVendor 根据id获取LotteryPrizeVendor记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryPrizeVendorService *LotteryPrizeVendorService)GetLotteryPrizeVendor(id uint) (lotteryPrizeVendor wclClassic.LotteryPrizeVendor, err error) {
	err = global.MustGetGlobalDBByDBName("wclClassic").Where("id = ?", id).First(&lotteryPrizeVendor).Error
	return
}

// GetLotteryPrizeVendorInfoList 分页获取LotteryPrizeVendor记录
// Author [piexlmax](https://github.com/piexlmax)
func (lotteryPrizeVendorService *LotteryPrizeVendorService)GetLotteryPrizeVendorInfoList(info wclClassicReq.LotteryPrizeVendorSearch) (list []wclClassic.LotteryPrizeVendor, total int64, err error) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
    // 创建db
	db := global.MustGetGlobalDBByDBName("wclClassic").Model(&wclClassic.LotteryPrizeVendor{})
    var lotteryPrizeVendors []wclClassic.LotteryPrizeVendor
    // 如果有条件搜索 下方会自动创建搜索语句
    if info.StartCreatedAt !=nil && info.EndCreatedAt !=nil {
     db = db.Where("created_at BETWEEN ? AND ?", info.StartCreatedAt, info.EndCreatedAt)
    }
	err = db.Count(&total).Error
	if err!=nil {
    	return
    }

	err = db.Limit(limit).Offset(offset).Find(&lotteryPrizeVendors).Error
	return  lotteryPrizeVendors, total, err
}
