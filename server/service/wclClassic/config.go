/*
 * @Author: Cheng Wei
 * @Date: 2023-07-19 10:52:57
 * @LastEditTime: 2023-08-22 12:21:27
 * @LastEditors: Cheng Wei
 * @Description:
 * @FilePath: \logs-admin\server\service\wclClassic\config.go
 * @GitHub: https://github.com/glegoo
 */
package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	wclClassicReq "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic/request"
)

type ConfigService struct {
}

// CreateConfig 创建Config记录
// Author [piexlmax](https://github.com/piexlmax)
func (configService *ConfigService) CreateConfig(config *wclClassic.Config, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Create(config).Error
	return err
}

// DeleteConfig 删除Config记录
// Author [piexlmax](https://github.com/piexlmax)
func (configService *ConfigService) DeleteConfig(config wclClassic.Config, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Delete(&config).Error
	return err
}

// DeleteConfigByIds 批量删除Config记录
// Author [piexlmax](https://github.com/piexlmax)
func (configService *ConfigService) DeleteConfigByIds(ids request.IdsReq, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Delete(&[]wclClassic.Config{}, "id in ?", ids.Ids).Error
	return err
}

// UpdateConfig 更新Config记录
// Author [piexlmax](https://github.com/piexlmax)
func (configService *ConfigService) UpdateConfig(config wclClassic.Config, game string) (err error) {
	err = global.MustGetGlobalDBByDBName(game).Save(&config).Error
	return err
}

// GetConfig 根据id获取Config记录
// Author [piexlmax](https://github.com/piexlmax)
func (configService *ConfigService) GetConfig(id uint, game string) (config wclClassic.Config, err error) {
	err = global.MustGetGlobalDBByDBName(game).Where("id = ?", id).First(&config).Error
	return
}

// GetConfigInfoList 分页获取Config记录
// Author [piexlmax](https://github.com/piexlmax)
func (configService *ConfigService) GetConfigInfoList(info wclClassicReq.ConfigSearch, game string) (list []wclClassic.Config, total int64, err error) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	// 创建db
	db := global.MustGetGlobalDBByDBName(game).Model(&wclClassic.Config{})
	var configs []wclClassic.Config
	// 如果有条件搜索 下方会自动创建搜索语句
	if info.StartCreatedAt != nil && info.EndCreatedAt != nil {
		db = db.Where("created_at BETWEEN ? AND ?", info.StartCreatedAt, info.EndCreatedAt)
	}
	err = db.Count(&total).Error
	if err != nil {
		return
	}

	err = db.Limit(limit).Offset(offset).Find(&configs).Error
	return configs, total, err
}
