/*
 * @Author: Cheng Wei
 * @Date: 2023-06-14 09:22:30
 * @LastEditTime: 2023-06-16 16:46:53
 * @LastEditors: Cheng Wei
 * @Description:
 * @FilePath: \logs-admin\server\model\wclClassic\article.go
 * @GitHub: https://github.com/glegoo
 */
// 自动生成模板Article
package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
)

// Article 结构体
type Article struct {
	global.GVA_MODEL
	Title     string `json:"title" form:"title" gorm:"column:title;comment:文章标题;"`
	Image     string `json:"image" form:"image" gorm:"column:image;comment:文章图片;"`
	Content   string `json:"content" form:"content" gorm:"type:mediumtext;column:content;comment:文章内容;"`
	Category  *int   `json:"category" form:"category" gorm:"column:category;comment:文章类别;"`
	CreatedBy uint   `gorm:"column:created_by;comment:创建者"`
	UpdatedBy uint   `gorm:"column:updated_by;comment:更新者"`
	DeletedBy uint   `gorm:"column:deleted_by;comment:删除者"`
}

// TableName Article 表名
func (Article) TableName() string {
	return "article"
}
