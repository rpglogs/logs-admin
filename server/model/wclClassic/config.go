// 自动生成模板Config
package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	
)

// Config 结构体
type Config struct {
      global.GVA_MODEL
      Key  string `json:"key" form:"key" gorm:"column:key;comment:键;"`
      Value  string `json:"value" form:"value" gorm:"column:value;comment:值;"`
}


// TableName Config 表名
func (Config) TableName() string {
  return "config"
}

