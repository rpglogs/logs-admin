/*
 * @Author: Cheng Wei
 * @Date: 2024-09-13 21:50:28
 * @LastEditTime: 2024-09-20 09:14:08
 * @LastEditors: Cheng Wei
 * @Description:
 * @FilePath: \logs-admin\server\model\wclClassic\cdkey.go
 * @GitHub: https://github.com/glegoo
 */
// 自动生成模板Cdkey
package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
)

// Cdkey 结构体
type Cdkey struct {
	global.GVA_MODEL
	Code    string   `json:"code" form:"code" gorm:"column:code;comment:兑换码;size:191;"`
	Status  int      `json:"status" form:"status" gorm:"column:status;comment:状态;size:20;"`
	Uid     *string  `json:"uid" form:"uid" gorm:"column:uid;comment:;size:191;"`
	User    *Account `json:"user" gorm:"foreignKey:Uid;references:Uuid"`
	PrizeId uint     `json:"prizeId" form:"prizeId" gorm:"column:prize_id;comment:;size:20;"`
	Prize   *Goods   `json:"prize" gorm:"foreignKey:PrizeId;"`
	Remark  string   `json:"remark" form:"remark" gorm:"column:remark;comment:备注;size:191;"`
}

// TableName Cdkey 表名
func (Cdkey) TableName() string {
	return "cdkey"
}
