/*
 * @Author: Cheng Wei
 * @Date: 2024-09-13 21:50:28
 * @LastEditTime: 2024-09-20 09:13:22
 * @LastEditors: Cheng Wei
 * @Description:
 * @FilePath: \logs-admin\server\model\wclClassic\request\cdkey.go
 * @GitHub: https://github.com/glegoo
 */
package request

import (
	"time"

	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
)

type CdkeySearch struct {
	wclClassic.Cdkey
	StartCreatedAt *time.Time `json:"startCreatedAt" form:"startCreatedAt"`
	EndCreatedAt   *time.Time `json:"endCreatedAt" form:"endCreatedAt"`
	request.PageInfo
}

type BatchCreateCdkeys struct {
	PrizeId uint   `json:"prizeId"` // 奖品ID
	Num     int    `json:"num"`     // 数量
	Remark  string `json:"remark"`  // 备注
}
