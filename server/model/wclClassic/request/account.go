package request

import (
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
	"time"
)

type AccountSearch struct{
    wclClassic.Account
    StartCreatedAt *time.Time `json:"startCreatedAt" form:"startCreatedAt"`
    EndCreatedAt   *time.Time `json:"endCreatedAt" form:"endCreatedAt"`
    StartLastLogin  *time.Time  `json:"startLastLogin" form:"startLastLogin"`
    EndLastLogin  *time.Time  `json:"endLastLogin" form:"endLastLogin"`
    StartVipExpiryTime  *time.Time  `json:"startVipExpiryTime" form:"startVipExpiryTime"`
    EndVipExpiryTime  *time.Time  `json:"endVipExpiryTime" form:"endVipExpiryTime"`
    request.PageInfo
}
