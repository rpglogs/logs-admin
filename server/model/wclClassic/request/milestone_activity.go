package request

import (
	"github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
	"time"
)

type MilestoneActivitySearch struct{
    wclClassic.MilestoneActivity
    StartCreatedAt *time.Time `json:"startCreatedAt" form:"startCreatedAt"`
    EndCreatedAt   *time.Time `json:"endCreatedAt" form:"endCreatedAt"`
    request.PageInfo
}
