/*
 * @Author: Cheng Wei
 * @Date: 2024-07-22 09:26:50
 * @LastEditTime: 2024-07-22 17:16:43
 * @LastEditors: Cheng Wei
 * @Description:
 * @FilePath: \logs-admin\server\model\wclClassic\account_address.go
 * @GitHub: https://github.com/glegoo
 */
package wclClassic

import "github.com/flipped-aurora/gin-vue-admin/server/global"

type AccountAddress struct {
	global.GVA_MODEL
	Name          string `json:"name"`
	Phone         string `json:"phone"`
	Province      string `json:"province"`
	City          string `json:"city"`
	District      string `json:"district"`
	DetailAddress string `json:"detail_address"`
	Postcode      string `json:"postcode"`
	Uid           string `json:"uid"`
}

func (AccountAddress) TableName() string {
	return "account_addresses"
}
