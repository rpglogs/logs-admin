// 自动生成模板CharacterBackgrounds
package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	
)

// CharacterBackgrounds 结构体
type CharacterBackgrounds struct {
      global.GVA_MODEL
      CharacterId  *int `json:"characterId" form:"characterId" gorm:"column:character_id;comment:;size:19;"`
      ImageUrl  string `json:"imageUrl" form:"imageUrl" gorm:"column:image_url;comment:;size:4294967295;"`
      Status  *int `json:"status" form:"status" gorm:"column:status;comment:;size:19;"`
      Uuid  string `json:"uuid" form:"uuid" gorm:"column:uuid;comment:;size:191;"`
      WclId  *int `json:"wclId" form:"wclId" gorm:"column:wcl_id;comment:;size:19;"`
}


// TableName CharacterBackgrounds 表名
func (CharacterBackgrounds) TableName() string {
  return "character_backgrounds"
}

