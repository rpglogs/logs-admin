// 自动生成模板Banner
package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
)

// Banner 结构体
type Banner struct {
	global.GVA_MODEL
	Name    string  `json:"name" form:"name" gorm:"column:name;comment:名称;"`
	Img     string  `json:"img" form:"img" gorm:"column:img;comment:图片地址;"`
	AppId   string  `json:"appId" form:"appId" gorm:"column:app_id;comment:AppId;"`
	Type    *int    `json:"type" form:"type" gorm:"column:type;comment:类型;"`
	Path    string  `json:"path" form:"path" gorm:"column:path;comment:路径;"`
	Disable *bool   `json:"disable" form:"disable" gorm:"column:disable;comment:禁用;"`
	Event   *string `json:"event" form:"event" gorm:"column:event;comment:事件;"`
}

// TableName Banner 表名
func (Banner) TableName() string {
	return "banner"
}
