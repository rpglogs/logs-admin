/*
 * @Author: Cheng Wei
 * @Date: 2024-07-01 09:50:30
 * @LastEditTime: 2024-07-02 10:51:19
 * @LastEditors: Cheng Wei
 * @Description:
 * @FilePath: \logs-admin\server\model\wclClassic\lottery_activity.go
 * @GitHub: https://github.com/glegoo
 */
// 自动生成模板LotteryActivity
package wclClassic

import (
	"time"

	"github.com/flipped-aurora/gin-vue-admin/server/global"
)

// LotteryActivity 结构体
type LotteryActivity struct {
	global.GVA_MODEL
	Name           string                  `json:"name" form:"name" gorm:"column:name;comment:;size:4294967295;"`
	ActivityType   *int                    `json:"activityType" form:"activityType" gorm:"column:activity_type;comment:;size:20;"`
	VendorId       *int                    `json:"vendorId" form:"vendorId" gorm:"column:vendor_id;comment:;size:20;"`
	Period         *float64                `json:"period" form:"period" gorm:"column:period;comment:;size:22;"`
	PointsRequired *int                    `json:"pointsRequired" form:"pointsRequired" gorm:"column:points_required;comment:;size:20;"`
	StartAt        *time.Time              `json:"startAt" form:"startAt" gorm:"column:start_at;comment:;"`
	EndAt          *time.Time              `json:"endAt" form:"endAt" gorm:"column:end_at;comment:;"`
	Status         *int                    `json:"status" form:"status" gorm:"column:status;comment:;size:20;"`
	Renewal        *bool                   `json:"renewal" form:"renewal" gorm:"column:renewal;comment:;size:20;"`
	Prizes         *[]LotteryActivityPrize `json:"prizes" gorm:"foreignKey:ActivityId"`
}

// TableName LotteryActivity 表名
func (LotteryActivity) TableName() string {
	return "lottery_activity"
}
