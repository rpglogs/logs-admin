/*
 * @Author: Cheng Wei
 * @Date: 2023-06-13 13:28:44
 * @LastEditTime: 2023-06-13 13:43:55
 * @LastEditors: Cheng Wei
 * @Description:
 * @FilePath: \logs-admin\server\model\wclClassic\account.go
 * @GitHub: https://github.com/glegoo
 */
// 自动生成模板Account
package wclClassic

import (
	"time"

	"gorm.io/gorm"
)

// Account 结构体
type Account struct {
	CreatedAt     time.Time      // 创建时间
	UpdatedAt     time.Time      // 更新时间
	DeletedAt     gorm.DeletedAt `gorm:"index" json:"-"` // 删除时间
	Uuid          string         `json:"uuid" form:"uuid" gorm:"column:uuid;comment:;size:191;"`
	LastLogin     *time.Time     `json:"lastLogin" form:"lastLogin" gorm:"column:last_login;comment:;"`
	Openid        string         `json:"openid" form:"openid" gorm:"column:openid;comment:;size:4294967295;"`
	Unionid       string         `json:"unionid" form:"unionid" gorm:"column:unionid;comment:;size:4294967295;"`
	IpAddress     string         `json:"ipAddress" form:"ipAddress" gorm:"column:ip_address;comment:;size:4294967295;"`
	TcbSynced     *int           `json:"tcbSynced" form:"tcbSynced" gorm:"column:tcb_synced;comment:;size:19;"`
	Dkp           *int           `json:"dkp" form:"dkp" gorm:"column:dkp;comment:;size:19;"`
	LastCheckIn   *time.Time     `json:"lastCheckIn" form:"lastCheckIn" gorm:"column:last_check_in;comment:;"`
	LastUnlock    *time.Time     `json:"lastUnlock" form:"lastUnlock" gorm:"column:last_unlock;comment:;"`
	VipExpiryTime *time.Time     `json:"vipExpiryTime" form:"vipExpiryTime" gorm:"column:vip_expiry_time;comment:;"`
	Name          string         `json:"name" form:"name" gorm:"column:name;comment:;size:4294967295;"`
	Avatar        string         `json:"avatar" form:"avatar" gorm:"column:avatar;comment:;size:4294967295;"`
	WclAccount    string         `json:"wclAccount" form:"wclAccount" gorm:"column:wcl_account;comment:;size:4294967295;"`
	Invalid       *int           `json:"invalid" form:"invalid" gorm:"column:invalid;comment:;size:19;"`
}

// TableName Account 表名
func (Account) TableName() string {
	return "account"
}
