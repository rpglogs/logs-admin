// 自动生成模板MilestoneActivity
package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"time"
)

// MilestoneActivity 结构体
type MilestoneActivity struct {
      global.GVA_MODEL
      Uid  string `json:"uid" form:"uid" gorm:"column:uid;comment:;size:191;"`
      Title  string `json:"title" form:"title" gorm:"column:title;comment:;size:191;"`
      Description  string `json:"description" form:"description" gorm:"column:description;comment:;size:4294967295;"`
      Deadline  *time.Time `json:"deadline" form:"deadline" gorm:"column:deadline;comment:;"`
      GameType  string `json:"gameType" form:"gameType" gorm:"column:game_type;comment:;size:191;"`
      PlayerLimitJson  string `json:"playerLimitJson" form:"playerLimitJson" gorm:"column:player_limit_json;comment:;size:4294967295;"`
      Region  string `json:"region" form:"region" gorm:"column:region;comment:;size:191;"`
      Server  string `json:"server" form:"server" gorm:"column:server;comment:;size:191;"`
      Faction  *int `json:"faction" form:"faction" gorm:"column:faction;comment:;size:20;"`
      ShowContact  *int `json:"showContact" form:"showContact" gorm:"column:show_contact;comment:;size:20;"`
      Size  *int `json:"size" form:"size" gorm:"column:size;comment:;size:20;"`
      IsPublic  *bool `json:"isPublic" form:"isPublic" gorm:"column:is_public;comment:;"`
      NotShare  *bool `json:"notShare" form:"notShare" gorm:"column:not_share;comment:;"`
}


// TableName MilestoneActivity 表名
func (MilestoneActivity) TableName() string {
  return "milestone_activity"
}

