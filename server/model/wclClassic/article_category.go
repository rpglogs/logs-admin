// 自动生成模板ArticleCategory
package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	
)

// ArticleCategory 结构体
type ArticleCategory struct {
      global.GVA_MODEL
      Name  string `json:"name" form:"name" gorm:"column:name;comment:名称;"`
}


// TableName ArticleCategory 表名
func (ArticleCategory) TableName() string {
  return "article_category"
}

