/*
 * @Author: Cheng Wei
 * @Date: 2024-07-22 09:14:43
 * @LastEditTime: 2024-07-22 09:46:40
 * @LastEditors: Cheng Wei
 * @Description:
 * @FilePath: \logs-admin\server\model\wclClassic\lottery_activity_win_record.go
 * @GitHub: https://github.com/glegoo
 */
// 自动生成模板LotteryActivityWinRecord
package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
)

// LotteryActivityWinRecord 结构体
type LotteryActivityWinRecord struct {
	global.GVA_MODEL
	ActivityId     *int                `json:"activityId" form:"activityId" gorm:"column:activity_id;comment:;size:20;"`
	Status         *int                `json:"status" form:"status" gorm:"column:status;comment:;size:20;"`
	PrizeId        *int                `json:"prizeId" form:"prizeId" gorm:"column:prize_id;comment:;size:20;"`
	Prize          *LotteryPrize       `json:"prize" form:"prize" gorm:"foreignKey:PrizeId;references:ID;comment:;size:20;"`
	VendorId       *int                `json:"vendorId" form:"vendorId" gorm:"column:vendor_id;comment:;size:20;"`
	Vendor         *LotteryPrizeVendor `json:"vendor" form:"vendor" gorm:"foreignKey:VendorId;references:ID;comment:;size:20;"`
	Uid            string              `json:"uid" form:"uid" gorm:"column:uid;comment:;size:191;"`
	User           *Account            `json:"user" form:"user" gorm:"foreignKey:Uid;references:Uuid;comment:;size:191;"`
	ExpressCompany string              `json:"expressCompany" form:"expressCompany" gorm:"column:express_company;comment:;size:191;"`
	ExpressNumber  string              `json:"expressNumber" form:"expressNumber" gorm:"column:express_number;comment:;size:191;"`
	PrizeOrderId   string              `json:"prizeOrderId" form:"prizeOrderId" gorm:"column:prize_order_id;comment:;size:191;"`
	AddressId      *int                `json:"addressId" form:"addressId" gorm:"column:address_id;comment:;size:20;"`
	Address        *AccountAddress     `json:"address" form:"address" gorm:"foreignKey:AddressId;references:ID;comment:;size:20;"`
}

// TableName LotteryActivityWinRecord 表名
func (LotteryActivityWinRecord) TableName() string {
	return "lottery_activity_win_record"
}
