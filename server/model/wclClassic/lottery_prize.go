/*
 * @Author: Cheng Wei
 * @Date: 2024-07-01 10:04:10
 * @LastEditTime: 2024-07-18 15:17:46
 * @LastEditors: Cheng Wei
 * @Description:
 * @FilePath: \logs-admin\server\model\wclClassic\lottery_prize.go
 * @GitHub: https://github.com/glegoo
 */
// 自动生成模板LotteryPrize
package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
)

// LotteryPrize 结构体
type LotteryPrize struct {
	global.GVA_MODEL
	Name         string   `json:"name" form:"name" gorm:"column:name;comment:;size:128;"`
	Url          string   `json:"url" form:"url" gorm:"column:url;comment:;size:1024;"`
	Desc         string   `json:"desc" form:"desc" gorm:"column:desc;comment:;size:1024;"`
	Thumbnail    string   `json:"thumbnail" form:"thumbnail" gorm:"column:thumbnail;comment:;size:128;"`
	DetailImages string   `json:"detailImages" form:"detailImages" gorm:"column:detail_images;comment:;size:1024;"`
	Price        *float64 `json:"price" form:"price" gorm:"column:price;comment:;size:22;"`
	VendorId     *int     `json:"vendorId" form:"vendorId" gorm:"column:vendor_id;comment:;size:20;"`
	Type         uint     `json:"type" form:"type" gorm:"column:type;comment:;size:20;"` // 1:快递 2:虚拟卡密 3:入群领取
}

// TableName LotteryPrize 表名
func (LotteryPrize) TableName() string {
	return "lottery_prize"
}
