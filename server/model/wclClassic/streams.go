// 自动生成模板Streams
package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	
)

// Streams 结构体
type Streams struct {
      global.GVA_MODEL
      AccountId  string `json:"accountId" form:"accountId" gorm:"column:account_id;comment:;size:4294967295;"`
      Type  *int `json:"type" form:"type" gorm:"column:type;comment:;size:19;"`
      RoomId  string `json:"roomId" form:"roomId" gorm:"column:room_id;comment:;size:4294967295;"`
      CharacterId  *int `json:"characterId" form:"characterId" gorm:"column:character_id;comment:;size:19;"`
      ClassId  *int `json:"classId" form:"classId" gorm:"column:class_id;comment:;size:19;"`
}


// TableName Streams 表名
func (Streams) TableName() string {
  return "streams"
}

