// 自动生成模板MilestoneReport
package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	
)

// MilestoneReport 结构体
type MilestoneReport struct {
      global.GVA_MODEL
      ActivityId  *int `json:"activityId" form:"activityId" gorm:"column:activity_id;comment:;size:20;"`
      ReportType  *int `json:"reportType" form:"reportType" gorm:"column:report_type;comment:;size:20;"`
      ReportContent  string `json:"reportContent" form:"reportContent" gorm:"column:report_content;comment:;size:4294967295;"`
      ReporterUid  string `json:"reporterUid" form:"reporterUid" gorm:"column:reporter_uid;comment:;size:191;"`
      Status  *int `json:"status" form:"status" gorm:"column:status;comment:;size:20;"`
      HandleResult  string `json:"handleResult" form:"handleResult" gorm:"column:handle_result;comment:;size:4294967295;"`
}


// TableName MilestoneReport 表名
func (MilestoneReport) TableName() string {
  return "milestone_report"
}

