/*
 * @Author: Cheng Wei
 * @Date: 2024-07-01 21:37:43
 * @LastEditTime: 2024-07-02 11:53:53
 * @LastEditors: Cheng Wei
 * @Description:
 * @FilePath: \logs-admin\server\model\wclClassic\lottery_activity_prize.go
 * @GitHub: https://github.com/glegoo
 */
package wclClassic

import "github.com/flipped-aurora/gin-vue-admin/server/global"

type LotteryActivityPrize struct {
	global.GVA_MODEL
	ActivityId uint         `json:"activityId" gorm:"index"` // 活动ID
	PrizeId    uint         `json:"prizeId" gorm:"index"`
	Prize      LotteryPrize `json:"prize" gorm:"foreignKey:PrizeId;references:id"`
	Num        int          `json:"num"`
}

func (LotteryActivityPrize) TableName() string {
	return "lottery_activity_prize"
}
