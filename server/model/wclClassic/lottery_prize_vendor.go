// 自动生成模板LotteryPrizeVendor
package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	
)

// LotteryPrizeVendor 结构体
type LotteryPrizeVendor struct {
      global.GVA_MODEL
      Name  string `json:"name" form:"name" gorm:"column:name;comment:;size:191;"`
      Phone  string `json:"phone" form:"phone" gorm:"column:phone;comment:;size:20;"`
      Email  string `json:"email" form:"email" gorm:"column:email;comment:;size:191;"`
      Remark  string `json:"remark" form:"remark" gorm:"column:remark;comment:;size:191;"`
}


// TableName LotteryPrizeVendor 表名
func (LotteryPrizeVendor) TableName() string {
  return "lottery_prize_vendor"
}

