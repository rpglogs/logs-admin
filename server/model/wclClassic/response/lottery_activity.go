/*
 * @Author: Cheng Wei
 * @Date: 2024-07-28 13:29:14
 * @LastEditTime: 2024-07-28 15:54:52
 * @LastEditors: Cheng Wei
 * @Description:
 * @FilePath: \logs-admin\server\model\wclClassic\response\lottery_activity.go
 * @GitHub: https://github.com/glegoo
 */
package response

import "github.com/flipped-aurora/gin-vue-admin/server/model/wclClassic"

type LotteryActivityInfo struct {
	wclClassic.LotteryActivity
	EntryCount int `json:"entryCount"`
}
