// 自动生成模板Goods
package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	
)

// Goods 结构体
type Goods struct {
      global.GVA_MODEL
      Type  string `json:"type" form:"type" gorm:"column:type;comment:;size:191;"`
      Description  string `json:"description" form:"description" gorm:"column:description;comment:;size:4294967295;"`
      Price  *int `json:"price" form:"price" gorm:"column:price;comment:;size:20;"`
      Days  *int `json:"days" form:"days" gorm:"column:days;comment:;size:19;"`
}


// TableName Goods 表名
func (Goods) TableName() string {
  return "goods"
}

