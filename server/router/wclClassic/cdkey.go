/*
 * @Author: Cheng Wei
 * @Date: 2024-09-15 17:44:14
 * @LastEditTime: 2024-09-16 11:12:39
 * @LastEditors: Cheng Wei
 * @Description:
 * @FilePath: \logs-admin\server\router\wclClassic\cdkey.go
 * @GitHub: https://github.com/glegoo
 */
package wclClassic

import (
	v1 "github.com/flipped-aurora/gin-vue-admin/server/api/v1"
	"github.com/flipped-aurora/gin-vue-admin/server/middleware"
	"github.com/gin-gonic/gin"
)

type CdkeyRouter struct {
}

// InitCdkeyRouter 初始化 Cdkey 路由信息
func (s *CdkeyRouter) InitCdkeyRouter(Router *gin.RouterGroup) {
	cdkeyRouter := Router.Group("cdkey").Use(middleware.OperationRecord())
	cdkeyRouterWithoutRecord := Router.Group("cdkey")
	var cdkeyApi = v1.ApiGroupApp.WclClassicApiGroup.CdkeyApi
	{
		cdkeyRouter.POST("createCdkey", cdkeyApi.CreateCdkey) // 新建Cdkey
		cdkeyRouter.POST("batchCreateCdkey", cdkeyApi.BatchCreateCdkeys)
		cdkeyRouter.DELETE("deleteCdkey", cdkeyApi.DeleteCdkey)           // 删除Cdkey
		cdkeyRouter.DELETE("deleteCdkeyByIds", cdkeyApi.DeleteCdkeyByIds) // 批量删除Cdkey
		cdkeyRouter.PUT("updateCdkey", cdkeyApi.UpdateCdkey)              // 更新Cdkey
	}
	{
		cdkeyRouterWithoutRecord.GET("findCdkey", cdkeyApi.FindCdkey)       // 根据ID获取Cdkey
		cdkeyRouterWithoutRecord.GET("getCdkeyList", cdkeyApi.GetCdkeyList) // 获取Cdkey列表
	}
}
