package wclClassic

type RouterGroup struct {
	AccountRouter
	ArticleRouter
	ArticleCategoryRouter
	BannerRouter
	ConfigRouter
	StreamsRouter
	LotteryActivityRouter
	LotteryPrizeVendorRouter
	LotteryPrizeRouter
	LotteryActivityWinRecordRouter
	CdkeyRouter
	GoodsRouter
	CharacterBackgroundsRouter
	MilestoneReportRouter
	MilestoneActivityRouter
}
