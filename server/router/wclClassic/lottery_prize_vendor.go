package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/api/v1"
	"github.com/flipped-aurora/gin-vue-admin/server/middleware"
	"github.com/gin-gonic/gin"
)

type LotteryPrizeVendorRouter struct {
}

// InitLotteryPrizeVendorRouter 初始化 LotteryPrizeVendor 路由信息
func (s *LotteryPrizeVendorRouter) InitLotteryPrizeVendorRouter(Router *gin.RouterGroup) {
	lotteryPrizeVendorRouter := Router.Group("lotteryPrizeVendor").Use(middleware.OperationRecord())
	lotteryPrizeVendorRouterWithoutRecord := Router.Group("lotteryPrizeVendor")
	var lotteryPrizeVendorApi = v1.ApiGroupApp.WclClassicApiGroup.LotteryPrizeVendorApi
	{
		lotteryPrizeVendorRouter.POST("createLotteryPrizeVendor", lotteryPrizeVendorApi.CreateLotteryPrizeVendor)   // 新建LotteryPrizeVendor
		lotteryPrizeVendorRouter.DELETE("deleteLotteryPrizeVendor", lotteryPrizeVendorApi.DeleteLotteryPrizeVendor) // 删除LotteryPrizeVendor
		lotteryPrizeVendorRouter.DELETE("deleteLotteryPrizeVendorByIds", lotteryPrizeVendorApi.DeleteLotteryPrizeVendorByIds) // 批量删除LotteryPrizeVendor
		lotteryPrizeVendorRouter.PUT("updateLotteryPrizeVendor", lotteryPrizeVendorApi.UpdateLotteryPrizeVendor)    // 更新LotteryPrizeVendor
	}
	{
		lotteryPrizeVendorRouterWithoutRecord.GET("findLotteryPrizeVendor", lotteryPrizeVendorApi.FindLotteryPrizeVendor)        // 根据ID获取LotteryPrizeVendor
		lotteryPrizeVendorRouterWithoutRecord.GET("getLotteryPrizeVendorList", lotteryPrizeVendorApi.GetLotteryPrizeVendorList)  // 获取LotteryPrizeVendor列表
	}
}
