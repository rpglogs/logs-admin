package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/api/v1"
	"github.com/flipped-aurora/gin-vue-admin/server/middleware"
	"github.com/gin-gonic/gin"
)

type LotteryPrizeRouter struct {
}

// InitLotteryPrizeRouter 初始化 LotteryPrize 路由信息
func (s *LotteryPrizeRouter) InitLotteryPrizeRouter(Router *gin.RouterGroup) {
	lotteryPrizeRouter := Router.Group("lotteryPrize").Use(middleware.OperationRecord())
	lotteryPrizeRouterWithoutRecord := Router.Group("lotteryPrize")
	var lotteryPrizeApi = v1.ApiGroupApp.WclClassicApiGroup.LotteryPrizeApi
	{
		lotteryPrizeRouter.POST("createLotteryPrize", lotteryPrizeApi.CreateLotteryPrize)   // 新建LotteryPrize
		lotteryPrizeRouter.DELETE("deleteLotteryPrize", lotteryPrizeApi.DeleteLotteryPrize) // 删除LotteryPrize
		lotteryPrizeRouter.DELETE("deleteLotteryPrizeByIds", lotteryPrizeApi.DeleteLotteryPrizeByIds) // 批量删除LotteryPrize
		lotteryPrizeRouter.PUT("updateLotteryPrize", lotteryPrizeApi.UpdateLotteryPrize)    // 更新LotteryPrize
	}
	{
		lotteryPrizeRouterWithoutRecord.GET("findLotteryPrize", lotteryPrizeApi.FindLotteryPrize)        // 根据ID获取LotteryPrize
		lotteryPrizeRouterWithoutRecord.GET("getLotteryPrizeList", lotteryPrizeApi.GetLotteryPrizeList)  // 获取LotteryPrize列表
	}
}
