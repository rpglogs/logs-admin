/*
 * @Author: Cheng Wei
 * @Date: 2023-07-19 10:52:57
 * @LastEditTime: 2023-08-22 11:42:58
 * @LastEditors: Cheng Wei
 * @Description:
 * @FilePath: \logs-admin\server\router\wclClassic\config.go
 * @GitHub: https://github.com/glegoo
 */
package wclClassic

import (
	v1 "github.com/flipped-aurora/gin-vue-admin/server/api/v1"
	"github.com/flipped-aurora/gin-vue-admin/server/middleware"
	"github.com/gin-gonic/gin"
)

type ConfigRouter struct {
}

// InitConfigRouter 初始化 Config 路由信息
func (s *ConfigRouter) InitConfigRouter(Router *gin.RouterGroup) {
	configRouter := Router.Group("config").Use(middleware.OperationRecord())
	configRouterWithoutRecord := Router.Group("config")
	var configApi = v1.ApiGroupApp.WclClassicApiGroup.ConfigApi
	{
		configRouter.POST("createConfig", configApi.CreateConfig)             // 新建Config
		configRouter.DELETE("deleteConfig", configApi.DeleteConfig)           // 删除Config
		configRouter.DELETE("deleteConfigByIds", configApi.DeleteConfigByIds) // 批量删除Config
		configRouter.PUT("updateConfig", configApi.UpdateConfig)              // 更新Config
	}
	{
		configRouterWithoutRecord.GET("findConfig", configApi.FindConfig)       // 根据ID获取Config
		configRouterWithoutRecord.GET("getConfigList", configApi.GetConfigList) // 获取Config列表
	}
}
