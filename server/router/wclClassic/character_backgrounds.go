package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/api/v1"
	"github.com/flipped-aurora/gin-vue-admin/server/middleware"
	"github.com/gin-gonic/gin"
)

type CharacterBackgroundsRouter struct {
}

// InitCharacterBackgroundsRouter 初始化 CharacterBackgrounds 路由信息
func (s *CharacterBackgroundsRouter) InitCharacterBackgroundsRouter(Router *gin.RouterGroup) {
	characterBackgroundsRouter := Router.Group("characterBackgrounds").Use(middleware.OperationRecord())
	characterBackgroundsRouterWithoutRecord := Router.Group("characterBackgrounds")
	var characterBackgroundsApi = v1.ApiGroupApp.WclClassicApiGroup.CharacterBackgroundsApi
	{
		characterBackgroundsRouter.POST("createCharacterBackgrounds", characterBackgroundsApi.CreateCharacterBackgrounds)   // 新建CharacterBackgrounds
		characterBackgroundsRouter.DELETE("deleteCharacterBackgrounds", characterBackgroundsApi.DeleteCharacterBackgrounds) // 删除CharacterBackgrounds
		characterBackgroundsRouter.DELETE("deleteCharacterBackgroundsByIds", characterBackgroundsApi.DeleteCharacterBackgroundsByIds) // 批量删除CharacterBackgrounds
		characterBackgroundsRouter.PUT("updateCharacterBackgrounds", characterBackgroundsApi.UpdateCharacterBackgrounds)    // 更新CharacterBackgrounds
	}
	{
		characterBackgroundsRouterWithoutRecord.GET("findCharacterBackgrounds", characterBackgroundsApi.FindCharacterBackgrounds)        // 根据ID获取CharacterBackgrounds
		characterBackgroundsRouterWithoutRecord.GET("getCharacterBackgroundsList", characterBackgroundsApi.GetCharacterBackgroundsList)  // 获取CharacterBackgrounds列表
	}
}
