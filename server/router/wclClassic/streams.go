package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/api/v1"
	"github.com/flipped-aurora/gin-vue-admin/server/middleware"
	"github.com/gin-gonic/gin"
)

type StreamsRouter struct {
}

// InitStreamsRouter 初始化 Streams 路由信息
func (s *StreamsRouter) InitStreamsRouter(Router *gin.RouterGroup) {
	streamsRouter := Router.Group("streams").Use(middleware.OperationRecord())
	streamsRouterWithoutRecord := Router.Group("streams")
	var streamsApi = v1.ApiGroupApp.WclClassicApiGroup.StreamsApi
	{
		streamsRouter.POST("createStreams", streamsApi.CreateStreams)   // 新建Streams
		streamsRouter.DELETE("deleteStreams", streamsApi.DeleteStreams) // 删除Streams
		streamsRouter.DELETE("deleteStreamsByIds", streamsApi.DeleteStreamsByIds) // 批量删除Streams
		streamsRouter.PUT("updateStreams", streamsApi.UpdateStreams)    // 更新Streams
	}
	{
		streamsRouterWithoutRecord.GET("findStreams", streamsApi.FindStreams)        // 根据ID获取Streams
		streamsRouterWithoutRecord.GET("getStreamsList", streamsApi.GetStreamsList)  // 获取Streams列表
	}
}
