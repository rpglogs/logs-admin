package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/api/v1"
	"github.com/flipped-aurora/gin-vue-admin/server/middleware"
	"github.com/gin-gonic/gin"
)

type MilestoneActivityRouter struct {
}

// InitMilestoneActivityRouter 初始化 MilestoneActivity 路由信息
func (s *MilestoneActivityRouter) InitMilestoneActivityRouter(Router *gin.RouterGroup) {
	milestoneActivityRouter := Router.Group("milestoneActivity").Use(middleware.OperationRecord())
	milestoneActivityRouterWithoutRecord := Router.Group("milestoneActivity")
	var milestoneActivityApi = v1.ApiGroupApp.WclClassicApiGroup.MilestoneActivityApi
	{
		milestoneActivityRouter.POST("createMilestoneActivity", milestoneActivityApi.CreateMilestoneActivity)   // 新建MilestoneActivity
		milestoneActivityRouter.DELETE("deleteMilestoneActivity", milestoneActivityApi.DeleteMilestoneActivity) // 删除MilestoneActivity
		milestoneActivityRouter.DELETE("deleteMilestoneActivityByIds", milestoneActivityApi.DeleteMilestoneActivityByIds) // 批量删除MilestoneActivity
		milestoneActivityRouter.PUT("updateMilestoneActivity", milestoneActivityApi.UpdateMilestoneActivity)    // 更新MilestoneActivity
	}
	{
		milestoneActivityRouterWithoutRecord.GET("findMilestoneActivity", milestoneActivityApi.FindMilestoneActivity)        // 根据ID获取MilestoneActivity
		milestoneActivityRouterWithoutRecord.GET("getMilestoneActivityList", milestoneActivityApi.GetMilestoneActivityList)  // 获取MilestoneActivity列表
	}
}
