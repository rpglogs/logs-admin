package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/api/v1"
	"github.com/flipped-aurora/gin-vue-admin/server/middleware"
	"github.com/gin-gonic/gin"
)

type LotteryActivityRouter struct {
}

// InitLotteryActivityRouter 初始化 LotteryActivity 路由信息
func (s *LotteryActivityRouter) InitLotteryActivityRouter(Router *gin.RouterGroup) {
	lotteryActivityRouter := Router.Group("lotteryActivity").Use(middleware.OperationRecord())
	lotteryActivityRouterWithoutRecord := Router.Group("lotteryActivity")
	var lotteryActivityApi = v1.ApiGroupApp.WclClassicApiGroup.LotteryActivityApi
	{
		lotteryActivityRouter.POST("createLotteryActivity", lotteryActivityApi.CreateLotteryActivity)   // 新建LotteryActivity
		lotteryActivityRouter.DELETE("deleteLotteryActivity", lotteryActivityApi.DeleteLotteryActivity) // 删除LotteryActivity
		lotteryActivityRouter.DELETE("deleteLotteryActivityByIds", lotteryActivityApi.DeleteLotteryActivityByIds) // 批量删除LotteryActivity
		lotteryActivityRouter.PUT("updateLotteryActivity", lotteryActivityApi.UpdateLotteryActivity)    // 更新LotteryActivity
	}
	{
		lotteryActivityRouterWithoutRecord.GET("findLotteryActivity", lotteryActivityApi.FindLotteryActivity)        // 根据ID获取LotteryActivity
		lotteryActivityRouterWithoutRecord.GET("getLotteryActivityList", lotteryActivityApi.GetLotteryActivityList)  // 获取LotteryActivity列表
	}
}
