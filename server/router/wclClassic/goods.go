package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/api/v1"
	"github.com/flipped-aurora/gin-vue-admin/server/middleware"
	"github.com/gin-gonic/gin"
)

type GoodsRouter struct {
}

// InitGoodsRouter 初始化 Goods 路由信息
func (s *GoodsRouter) InitGoodsRouter(Router *gin.RouterGroup) {
	goodsRouter := Router.Group("goods").Use(middleware.OperationRecord())
	goodsRouterWithoutRecord := Router.Group("goods")
	var goodsApi = v1.ApiGroupApp.WclClassicApiGroup.GoodsApi
	{
		goodsRouter.POST("createGoods", goodsApi.CreateGoods)   // 新建Goods
		goodsRouter.DELETE("deleteGoods", goodsApi.DeleteGoods) // 删除Goods
		goodsRouter.DELETE("deleteGoodsByIds", goodsApi.DeleteGoodsByIds) // 批量删除Goods
		goodsRouter.PUT("updateGoods", goodsApi.UpdateGoods)    // 更新Goods
	}
	{
		goodsRouterWithoutRecord.GET("findGoods", goodsApi.FindGoods)        // 根据ID获取Goods
		goodsRouterWithoutRecord.GET("getGoodsList", goodsApi.GetGoodsList)  // 获取Goods列表
	}
}
