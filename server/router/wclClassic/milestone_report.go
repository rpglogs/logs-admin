package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/api/v1"
	"github.com/flipped-aurora/gin-vue-admin/server/middleware"
	"github.com/gin-gonic/gin"
)

type MilestoneReportRouter struct {
}

// InitMilestoneReportRouter 初始化 MilestoneReport 路由信息
func (s *MilestoneReportRouter) InitMilestoneReportRouter(Router *gin.RouterGroup) {
	milestoneReportRouter := Router.Group("milestoneReport").Use(middleware.OperationRecord())
	milestoneReportRouterWithoutRecord := Router.Group("milestoneReport")
	var milestoneReportApi = v1.ApiGroupApp.WclClassicApiGroup.MilestoneReportApi
	{
		milestoneReportRouter.POST("createMilestoneReport", milestoneReportApi.CreateMilestoneReport)   // 新建MilestoneReport
		milestoneReportRouter.DELETE("deleteMilestoneReport", milestoneReportApi.DeleteMilestoneReport) // 删除MilestoneReport
		milestoneReportRouter.DELETE("deleteMilestoneReportByIds", milestoneReportApi.DeleteMilestoneReportByIds) // 批量删除MilestoneReport
		milestoneReportRouter.PUT("updateMilestoneReport", milestoneReportApi.UpdateMilestoneReport)    // 更新MilestoneReport
	}
	{
		milestoneReportRouterWithoutRecord.GET("findMilestoneReport", milestoneReportApi.FindMilestoneReport)        // 根据ID获取MilestoneReport
		milestoneReportRouterWithoutRecord.GET("getMilestoneReportList", milestoneReportApi.GetMilestoneReportList)  // 获取MilestoneReport列表
	}
}
