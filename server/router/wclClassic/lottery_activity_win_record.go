package wclClassic

import (
	"github.com/flipped-aurora/gin-vue-admin/server/api/v1"
	"github.com/flipped-aurora/gin-vue-admin/server/middleware"
	"github.com/gin-gonic/gin"
)

type LotteryActivityWinRecordRouter struct {
}

// InitLotteryActivityWinRecordRouter 初始化 LotteryActivityWinRecord 路由信息
func (s *LotteryActivityWinRecordRouter) InitLotteryActivityWinRecordRouter(Router *gin.RouterGroup) {
	lotteryActivityWinRecordRouter := Router.Group("lotteryActivityWinRecord").Use(middleware.OperationRecord())
	lotteryActivityWinRecordRouterWithoutRecord := Router.Group("lotteryActivityWinRecord")
	var lotteryActivityWinRecordApi = v1.ApiGroupApp.WclClassicApiGroup.LotteryActivityWinRecordApi
	{
		lotteryActivityWinRecordRouter.POST("createLotteryActivityWinRecord", lotteryActivityWinRecordApi.CreateLotteryActivityWinRecord)   // 新建LotteryActivityWinRecord
		lotteryActivityWinRecordRouter.DELETE("deleteLotteryActivityWinRecord", lotteryActivityWinRecordApi.DeleteLotteryActivityWinRecord) // 删除LotteryActivityWinRecord
		lotteryActivityWinRecordRouter.DELETE("deleteLotteryActivityWinRecordByIds", lotteryActivityWinRecordApi.DeleteLotteryActivityWinRecordByIds) // 批量删除LotteryActivityWinRecord
		lotteryActivityWinRecordRouter.PUT("updateLotteryActivityWinRecord", lotteryActivityWinRecordApi.UpdateLotteryActivityWinRecord)    // 更新LotteryActivityWinRecord
	}
	{
		lotteryActivityWinRecordRouterWithoutRecord.GET("findLotteryActivityWinRecord", lotteryActivityWinRecordApi.FindLotteryActivityWinRecord)        // 根据ID获取LotteryActivityWinRecord
		lotteryActivityWinRecordRouterWithoutRecord.GET("getLotteryActivityWinRecordList", lotteryActivityWinRecordApi.GetLotteryActivityWinRecordList)  // 获取LotteryActivityWinRecord列表
	}
}
