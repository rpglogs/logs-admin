<!--
 * @Author: Cheng Wei
 * @Date: 2023-11-13 10:04:26
 * @LastEditTime: 2024-07-22 23:22:40
 * @LastEditors: Cheng Wei
 * @Description: 
 * @FilePath: \logs-admin\README.md
 * @GitHub: https://github.com/glegoo
-->
# 添加新模块流程
1. 如果有数据中有枚举类型，先在`超级管理员->字典管理`中添加枚举类型
2. `系统工具->代码生成器`点击上方的`点这里从现有数据库创建代码`，选择对应的表，生成代码
3. 为生成的表格配置package，业务库
4. 修改field的中文名为更加易懂的名字
5. 为field勾选是否必填
6. 枚举类型的field，在`高级编辑`中关联字典
7. 为field配置搜索类型
8. 点击生成代码时，服务端不要开启air等自动编译，否则会报错
9. 生成代码后，服务端gorm.go需要修改 类似 xxx->xxxModles
10. 重新启动服务端
11. 在`超级管理员->菜单管理`中添加菜单
12. 在`超级管理员->角色管理`中添加角色权限
# 部署到正式服务器流程
1. 选择性将已有的wcl可用表数据/格式导入到正式服务器数据库，新增的表结构尽量直接导入正式服中，避免自动生成出现问题
2. 将admin中的`sys_apis` `sys_base_menus` `sys_dictionaries` `sys_dictionaries_details`表复制粘贴至正式服admin库中
3. 正式服recruit服务更新重启，自动化建表
4. 正式服admin服务更新重启
5. 正式服admin web端打包上传
