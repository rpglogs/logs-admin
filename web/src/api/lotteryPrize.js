import service from '@/utils/request'

// @Tags LotteryPrize
// @Summary 创建LotteryPrize
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.LotteryPrize true "创建LotteryPrize"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /lotteryPrize/createLotteryPrize [post]
export const createLotteryPrize = (data) => {
  return service({
    url: '/lotteryPrize/createLotteryPrize',
    method: 'post',
    data
  })
}

// @Tags LotteryPrize
// @Summary 删除LotteryPrize
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.LotteryPrize true "删除LotteryPrize"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /lotteryPrize/deleteLotteryPrize [delete]
export const deleteLotteryPrize = (data) => {
  return service({
    url: '/lotteryPrize/deleteLotteryPrize',
    method: 'delete',
    data
  })
}

// @Tags LotteryPrize
// @Summary 删除LotteryPrize
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除LotteryPrize"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /lotteryPrize/deleteLotteryPrize [delete]
export const deleteLotteryPrizeByIds = (data) => {
  return service({
    url: '/lotteryPrize/deleteLotteryPrizeByIds',
    method: 'delete',
    data
  })
}

// @Tags LotteryPrize
// @Summary 更新LotteryPrize
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.LotteryPrize true "更新LotteryPrize"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /lotteryPrize/updateLotteryPrize [put]
export const updateLotteryPrize = (data) => {
  return service({
    url: '/lotteryPrize/updateLotteryPrize',
    method: 'put',
    data
  })
}

// @Tags LotteryPrize
// @Summary 用id查询LotteryPrize
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query model.LotteryPrize true "用id查询LotteryPrize"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /lotteryPrize/findLotteryPrize [get]
export const findLotteryPrize = (params) => {
  return service({
    url: '/lotteryPrize/findLotteryPrize',
    method: 'get',
    params
  })
}

// @Tags LotteryPrize
// @Summary 分页获取LotteryPrize列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query request.PageInfo true "分页获取LotteryPrize列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /lotteryPrize/getLotteryPrizeList [get]
export const getLotteryPrizeList = (params) => {
  return service({
    url: '/lotteryPrize/getLotteryPrizeList',
    method: 'get',
    params
  })
}
