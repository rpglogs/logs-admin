import service from '@/utils/request'

// @Tags MilestoneReport
// @Summary 创建MilestoneReport
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.MilestoneReport true "创建MilestoneReport"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /milestoneReport/createMilestoneReport [post]
export const createMilestoneReport = (data) => {
  return service({
    url: '/milestoneReport/createMilestoneReport',
    method: 'post',
    data
  })
}

// @Tags MilestoneReport
// @Summary 删除MilestoneReport
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.MilestoneReport true "删除MilestoneReport"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /milestoneReport/deleteMilestoneReport [delete]
export const deleteMilestoneReport = (data) => {
  return service({
    url: '/milestoneReport/deleteMilestoneReport',
    method: 'delete',
    data
  })
}

// @Tags MilestoneReport
// @Summary 删除MilestoneReport
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除MilestoneReport"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /milestoneReport/deleteMilestoneReport [delete]
export const deleteMilestoneReportByIds = (data) => {
  return service({
    url: '/milestoneReport/deleteMilestoneReportByIds',
    method: 'delete',
    data
  })
}

// @Tags MilestoneReport
// @Summary 更新MilestoneReport
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.MilestoneReport true "更新MilestoneReport"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /milestoneReport/updateMilestoneReport [put]
export const updateMilestoneReport = (data) => {
  return service({
    url: '/milestoneReport/updateMilestoneReport',
    method: 'put',
    data
  })
}

// @Tags MilestoneReport
// @Summary 用id查询MilestoneReport
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query model.MilestoneReport true "用id查询MilestoneReport"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /milestoneReport/findMilestoneReport [get]
export const findMilestoneReport = (params) => {
  return service({
    url: '/milestoneReport/findMilestoneReport',
    method: 'get',
    params
  })
}

// @Tags MilestoneReport
// @Summary 分页获取MilestoneReport列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query request.PageInfo true "分页获取MilestoneReport列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /milestoneReport/getMilestoneReportList [get]
export const getMilestoneReportList = (params) => {
  return service({
    url: '/milestoneReport/getMilestoneReportList',
    method: 'get',
    params
  })
}
