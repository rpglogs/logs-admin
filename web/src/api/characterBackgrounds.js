import service from '@/utils/request'

// @Tags CharacterBackgrounds
// @Summary 创建CharacterBackgrounds
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CharacterBackgrounds true "创建CharacterBackgrounds"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /characterBackgrounds/createCharacterBackgrounds [post]
export const createCharacterBackgrounds = (data) => {
  return service({
    url: '/characterBackgrounds/createCharacterBackgrounds',
    method: 'post',
    data
  })
}

// @Tags CharacterBackgrounds
// @Summary 删除CharacterBackgrounds
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CharacterBackgrounds true "删除CharacterBackgrounds"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /characterBackgrounds/deleteCharacterBackgrounds [delete]
export const deleteCharacterBackgrounds = (data) => {
  return service({
    url: '/characterBackgrounds/deleteCharacterBackgrounds',
    method: 'delete',
    data
  })
}

// @Tags CharacterBackgrounds
// @Summary 删除CharacterBackgrounds
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除CharacterBackgrounds"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /characterBackgrounds/deleteCharacterBackgrounds [delete]
export const deleteCharacterBackgroundsByIds = (data) => {
  return service({
    url: '/characterBackgrounds/deleteCharacterBackgroundsByIds',
    method: 'delete',
    data
  })
}

// @Tags CharacterBackgrounds
// @Summary 更新CharacterBackgrounds
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CharacterBackgrounds true "更新CharacterBackgrounds"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /characterBackgrounds/updateCharacterBackgrounds [put]
export const updateCharacterBackgrounds = (data) => {
  return service({
    url: '/characterBackgrounds/updateCharacterBackgrounds',
    method: 'put',
    data
  })
}

// @Tags CharacterBackgrounds
// @Summary 用id查询CharacterBackgrounds
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query model.CharacterBackgrounds true "用id查询CharacterBackgrounds"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /characterBackgrounds/findCharacterBackgrounds [get]
export const findCharacterBackgrounds = (params) => {
  return service({
    url: '/characterBackgrounds/findCharacterBackgrounds',
    method: 'get',
    params
  })
}

// @Tags CharacterBackgrounds
// @Summary 分页获取CharacterBackgrounds列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query request.PageInfo true "分页获取CharacterBackgrounds列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /characterBackgrounds/getCharacterBackgroundsList [get]
export const getCharacterBackgroundsList = (params) => {
  return service({
    url: '/characterBackgrounds/getCharacterBackgroundsList',
    method: 'get',
    params
  })
}
