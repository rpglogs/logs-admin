import service from '@/utils/request'

// @Tags MilestoneActivity
// @Summary 创建MilestoneActivity
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.MilestoneActivity true "创建MilestoneActivity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /milestoneActivity/createMilestoneActivity [post]
export const createMilestoneActivity = (data) => {
  return service({
    url: '/milestoneActivity/createMilestoneActivity',
    method: 'post',
    data
  })
}

// @Tags MilestoneActivity
// @Summary 删除MilestoneActivity
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.MilestoneActivity true "删除MilestoneActivity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /milestoneActivity/deleteMilestoneActivity [delete]
export const deleteMilestoneActivity = (data) => {
  return service({
    url: '/milestoneActivity/deleteMilestoneActivity',
    method: 'delete',
    data
  })
}

// @Tags MilestoneActivity
// @Summary 删除MilestoneActivity
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除MilestoneActivity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /milestoneActivity/deleteMilestoneActivity [delete]
export const deleteMilestoneActivityByIds = (data) => {
  return service({
    url: '/milestoneActivity/deleteMilestoneActivityByIds',
    method: 'delete',
    data
  })
}

// @Tags MilestoneActivity
// @Summary 更新MilestoneActivity
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.MilestoneActivity true "更新MilestoneActivity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /milestoneActivity/updateMilestoneActivity [put]
export const updateMilestoneActivity = (data) => {
  return service({
    url: '/milestoneActivity/updateMilestoneActivity',
    method: 'put',
    data
  })
}

// @Tags MilestoneActivity
// @Summary 用id查询MilestoneActivity
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query model.MilestoneActivity true "用id查询MilestoneActivity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /milestoneActivity/findMilestoneActivity [get]
export const findMilestoneActivity = (params) => {
  return service({
    url: '/milestoneActivity/findMilestoneActivity',
    method: 'get',
    params
  })
}

// @Tags MilestoneActivity
// @Summary 分页获取MilestoneActivity列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query request.PageInfo true "分页获取MilestoneActivity列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /milestoneActivity/getMilestoneActivityList [get]
export const getMilestoneActivityList = (params) => {
  return service({
    url: '/milestoneActivity/getMilestoneActivityList',
    method: 'get',
    params
  })
}
