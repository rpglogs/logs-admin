import service from '@/utils/request'

// @Tags ArticleCategory
// @Summary 创建ArticleCategory
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.ArticleCategory true "创建ArticleCategory"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /articleCategory/createArticleCategory [post]
export const createArticleCategory = (data) => {
  return service({
    url: '/articleCategory/createArticleCategory',
    method: 'post',
    data
  })
}

// @Tags ArticleCategory
// @Summary 删除ArticleCategory
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.ArticleCategory true "删除ArticleCategory"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /articleCategory/deleteArticleCategory [delete]
export const deleteArticleCategory = (data) => {
  return service({
    url: '/articleCategory/deleteArticleCategory',
    method: 'delete',
    data
  })
}

// @Tags ArticleCategory
// @Summary 删除ArticleCategory
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除ArticleCategory"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /articleCategory/deleteArticleCategory [delete]
export const deleteArticleCategoryByIds = (data) => {
  return service({
    url: '/articleCategory/deleteArticleCategoryByIds',
    method: 'delete',
    data
  })
}

// @Tags ArticleCategory
// @Summary 更新ArticleCategory
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.ArticleCategory true "更新ArticleCategory"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /articleCategory/updateArticleCategory [put]
export const updateArticleCategory = (data) => {
  return service({
    url: '/articleCategory/updateArticleCategory',
    method: 'put',
    data
  })
}

// @Tags ArticleCategory
// @Summary 用id查询ArticleCategory
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query model.ArticleCategory true "用id查询ArticleCategory"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /articleCategory/findArticleCategory [get]
export const findArticleCategory = (params) => {
  return service({
    url: '/articleCategory/findArticleCategory',
    method: 'get',
    params
  })
}

// @Tags ArticleCategory
// @Summary 分页获取ArticleCategory列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query request.PageInfo true "分页获取ArticleCategory列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /articleCategory/getArticleCategoryList [get]
export const getArticleCategoryList = (params) => {
  return service({
    url: '/articleCategory/getArticleCategoryList',
    method: 'get',
    params
  })
}
