import service from '@/utils/request'

// @Tags Streams
// @Summary 创建Streams
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.Streams true "创建Streams"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /streams/createStreams [post]
export const createStreams = (data) => {
  return service({
    url: '/streams/createStreams',
    method: 'post',
    data
  })
}

// @Tags Streams
// @Summary 删除Streams
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.Streams true "删除Streams"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /streams/deleteStreams [delete]
export const deleteStreams = (data) => {
  return service({
    url: '/streams/deleteStreams',
    method: 'delete',
    data
  })
}

// @Tags Streams
// @Summary 删除Streams
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除Streams"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /streams/deleteStreams [delete]
export const deleteStreamsByIds = (data) => {
  return service({
    url: '/streams/deleteStreamsByIds',
    method: 'delete',
    data
  })
}

// @Tags Streams
// @Summary 更新Streams
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.Streams true "更新Streams"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /streams/updateStreams [put]
export const updateStreams = (data) => {
  return service({
    url: '/streams/updateStreams',
    method: 'put',
    data
  })
}

// @Tags Streams
// @Summary 用id查询Streams
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query model.Streams true "用id查询Streams"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /streams/findStreams [get]
export const findStreams = (params) => {
  return service({
    url: '/streams/findStreams',
    method: 'get',
    params
  })
}

// @Tags Streams
// @Summary 分页获取Streams列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query request.PageInfo true "分页获取Streams列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /streams/getStreamsList [get]
export const getStreamsList = (params) => {
  return service({
    url: '/streams/getStreamsList',
    method: 'get',
    params
  })
}
