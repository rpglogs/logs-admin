import service from '@/utils/request'

// @Tags LotteryPrizeVendor
// @Summary 创建LotteryPrizeVendor
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.LotteryPrizeVendor true "创建LotteryPrizeVendor"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /lotteryPrizeVendor/createLotteryPrizeVendor [post]
export const createLotteryPrizeVendor = (data) => {
  return service({
    url: '/lotteryPrizeVendor/createLotteryPrizeVendor',
    method: 'post',
    data
  })
}

// @Tags LotteryPrizeVendor
// @Summary 删除LotteryPrizeVendor
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.LotteryPrizeVendor true "删除LotteryPrizeVendor"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /lotteryPrizeVendor/deleteLotteryPrizeVendor [delete]
export const deleteLotteryPrizeVendor = (data) => {
  return service({
    url: '/lotteryPrizeVendor/deleteLotteryPrizeVendor',
    method: 'delete',
    data
  })
}

// @Tags LotteryPrizeVendor
// @Summary 删除LotteryPrizeVendor
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除LotteryPrizeVendor"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /lotteryPrizeVendor/deleteLotteryPrizeVendor [delete]
export const deleteLotteryPrizeVendorByIds = (data) => {
  return service({
    url: '/lotteryPrizeVendor/deleteLotteryPrizeVendorByIds',
    method: 'delete',
    data
  })
}

// @Tags LotteryPrizeVendor
// @Summary 更新LotteryPrizeVendor
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.LotteryPrizeVendor true "更新LotteryPrizeVendor"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /lotteryPrizeVendor/updateLotteryPrizeVendor [put]
export const updateLotteryPrizeVendor = (data) => {
  return service({
    url: '/lotteryPrizeVendor/updateLotteryPrizeVendor',
    method: 'put',
    data
  })
}

// @Tags LotteryPrizeVendor
// @Summary 用id查询LotteryPrizeVendor
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query model.LotteryPrizeVendor true "用id查询LotteryPrizeVendor"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /lotteryPrizeVendor/findLotteryPrizeVendor [get]
export const findLotteryPrizeVendor = (params) => {
  return service({
    url: '/lotteryPrizeVendor/findLotteryPrizeVendor',
    method: 'get',
    params
  })
}

// @Tags LotteryPrizeVendor
// @Summary 分页获取LotteryPrizeVendor列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query request.PageInfo true "分页获取LotteryPrizeVendor列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /lotteryPrizeVendor/getLotteryPrizeVendorList [get]
export const getLotteryPrizeVendorList = (params) => {
  return service({
    url: '/lotteryPrizeVendor/getLotteryPrizeVendorList',
    method: 'get',
    params
  })
}
