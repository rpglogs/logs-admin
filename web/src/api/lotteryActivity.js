import service from '@/utils/request'

// @Tags LotteryActivity
// @Summary 创建LotteryActivity
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.LotteryActivity true "创建LotteryActivity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /lotteryActivity/createLotteryActivity [post]
export const createLotteryActivity = (data) => {
  return service({
    url: '/lotteryActivity/createLotteryActivity',
    method: 'post',
    data
  })
}

// @Tags LotteryActivity
// @Summary 删除LotteryActivity
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.LotteryActivity true "删除LotteryActivity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /lotteryActivity/deleteLotteryActivity [delete]
export const deleteLotteryActivity = (data) => {
  return service({
    url: '/lotteryActivity/deleteLotteryActivity',
    method: 'delete',
    data
  })
}

// @Tags LotteryActivity
// @Summary 删除LotteryActivity
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除LotteryActivity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /lotteryActivity/deleteLotteryActivity [delete]
export const deleteLotteryActivityByIds = (data) => {
  return service({
    url: '/lotteryActivity/deleteLotteryActivityByIds',
    method: 'delete',
    data
  })
}

// @Tags LotteryActivity
// @Summary 更新LotteryActivity
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.LotteryActivity true "更新LotteryActivity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /lotteryActivity/updateLotteryActivity [put]
export const updateLotteryActivity = (data) => {
  return service({
    url: '/lotteryActivity/updateLotteryActivity',
    method: 'put',
    data
  })
}

// @Tags LotteryActivity
// @Summary 用id查询LotteryActivity
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query model.LotteryActivity true "用id查询LotteryActivity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /lotteryActivity/findLotteryActivity [get]
export const findLotteryActivity = (params) => {
  return service({
    url: '/lotteryActivity/findLotteryActivity',
    method: 'get',
    params
  })
}

// @Tags LotteryActivity
// @Summary 分页获取LotteryActivity列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query request.PageInfo true "分页获取LotteryActivity列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /lotteryActivity/getLotteryActivityList [get]
export const getLotteryActivityList = (params) => {
  return service({
    url: '/lotteryActivity/getLotteryActivityList',
    method: 'get',
    params
  })
}
