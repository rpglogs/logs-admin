import service from '@/utils/request'

// @Tags LotteryActivityWinRecord
// @Summary 创建LotteryActivityWinRecord
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.LotteryActivityWinRecord true "创建LotteryActivityWinRecord"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /lotteryActivityWinRecord/createLotteryActivityWinRecord [post]
export const createLotteryActivityWinRecord = (data) => {
  return service({
    url: '/lotteryActivityWinRecord/createLotteryActivityWinRecord',
    method: 'post',
    data
  })
}

// @Tags LotteryActivityWinRecord
// @Summary 删除LotteryActivityWinRecord
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.LotteryActivityWinRecord true "删除LotteryActivityWinRecord"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /lotteryActivityWinRecord/deleteLotteryActivityWinRecord [delete]
export const deleteLotteryActivityWinRecord = (data) => {
  return service({
    url: '/lotteryActivityWinRecord/deleteLotteryActivityWinRecord',
    method: 'delete',
    data
  })
}

// @Tags LotteryActivityWinRecord
// @Summary 删除LotteryActivityWinRecord
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除LotteryActivityWinRecord"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /lotteryActivityWinRecord/deleteLotteryActivityWinRecord [delete]
export const deleteLotteryActivityWinRecordByIds = (data) => {
  return service({
    url: '/lotteryActivityWinRecord/deleteLotteryActivityWinRecordByIds',
    method: 'delete',
    data
  })
}

// @Tags LotteryActivityWinRecord
// @Summary 更新LotteryActivityWinRecord
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.LotteryActivityWinRecord true "更新LotteryActivityWinRecord"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /lotteryActivityWinRecord/updateLotteryActivityWinRecord [put]
export const updateLotteryActivityWinRecord = (data) => {
  return service({
    url: '/lotteryActivityWinRecord/updateLotteryActivityWinRecord',
    method: 'put',
    data
  })
}

// @Tags LotteryActivityWinRecord
// @Summary 用id查询LotteryActivityWinRecord
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query model.LotteryActivityWinRecord true "用id查询LotteryActivityWinRecord"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /lotteryActivityWinRecord/findLotteryActivityWinRecord [get]
export const findLotteryActivityWinRecord = (params) => {
  return service({
    url: '/lotteryActivityWinRecord/findLotteryActivityWinRecord',
    method: 'get',
    params
  })
}

// @Tags LotteryActivityWinRecord
// @Summary 分页获取LotteryActivityWinRecord列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query request.PageInfo true "分页获取LotteryActivityWinRecord列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /lotteryActivityWinRecord/getLotteryActivityWinRecordList [get]
export const getLotteryActivityWinRecordList = (params) => {
  return service({
    url: '/lotteryActivityWinRecord/getLotteryActivityWinRecordList',
    method: 'get',
    params
  })
}
