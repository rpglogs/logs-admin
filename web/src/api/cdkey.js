import service from '@/utils/request'

// @Tags Cdkey
// @Summary 创建Cdkey
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.Cdkey true "创建Cdkey"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cdkey/createCdkey [post]
export const createCdkey = (data) => {
  return service({
    url: '/cdkey/createCdkey',
    method: 'post',
    data,
  })
}

export const batchCreateCdkey = (data) => {
  return service({
    url: '/cdkey/batchCreateCdkey',
    method: 'post',
    data,
  })
}

// @Tags Cdkey
// @Summary 删除Cdkey
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.Cdkey true "删除Cdkey"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /cdkey/deleteCdkey [delete]
export const deleteCdkey = (data) => {
  return service({
    url: '/cdkey/deleteCdkey',
    method: 'delete',
    data,
  })
}

// @Tags Cdkey
// @Summary 删除Cdkey
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除Cdkey"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /cdkey/deleteCdkey [delete]
export const deleteCdkeyByIds = (data) => {
  return service({
    url: '/cdkey/deleteCdkeyByIds',
    method: 'delete',
    data,
  })
}

// @Tags Cdkey
// @Summary 更新Cdkey
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.Cdkey true "更新Cdkey"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /cdkey/updateCdkey [put]
export const updateCdkey = (data) => {
  return service({
    url: '/cdkey/updateCdkey',
    method: 'put',
    data,
  })
}

// @Tags Cdkey
// @Summary 用id查询Cdkey
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query model.Cdkey true "用id查询Cdkey"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /cdkey/findCdkey [get]
export const findCdkey = (params) => {
  return service({
    url: '/cdkey/findCdkey',
    method: 'get',
    params,
  })
}

// @Tags Cdkey
// @Summary 分页获取Cdkey列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query request.PageInfo true "分页获取Cdkey列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cdkey/getCdkeyList [get]
export const getCdkeyList = (params) => {
  return service({
    url: '/cdkey/getCdkeyList',
    method: 'get',
    params,
  })
}
