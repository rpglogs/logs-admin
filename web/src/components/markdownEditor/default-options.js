// doc: https://nhnent.github.io/tui.editor/api/latest/ToastUIEditor.html#ToastUIEditor
// 默认选项
export default {
  minHeight: '200px', // 最小高度
  previewStyle: 'vertical', // 预览风格   vertical 垂直
  useCommandShortcut: true, // 使用命令快捷键
  useDefaultHTMLSanitizer: true, // 使用默认html清洁
  usageStatistics: false, // 使用情况统计
  hideModeSwitch: false, // 隐藏模式开关
  toolbarItems: [
    ['heading', 'bold', 'italic', 'strike'],
    ['hr', 'quote'],
    ['ul', 'ol', 'task', 'indent', 'outdent'],
    ['table', 'image', 'link'],
    ['code', 'codeblock'],
  ],
}
